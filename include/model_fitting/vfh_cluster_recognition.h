#ifndef _vfh_cluster_recognition_h
#define _vfh_cluster_recognition_h

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/common/common.h>
#include <pcl/common/transforms.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/console/parse.h>
#include <pcl/console/print.h>
#include <pcl/io/pcd_io.h>
#include <boost/filesystem.hpp>
#include <flann/flann.h>
#include <flann/io/hdf5.h>
#include <fstream>
#include <boost/filesystem.hpp>
#include "typedefs.h"

typedef std::pair<std::string, std::vector<float> > vfh_model;

class VFHClusterRecognition
{
	public:
		VFHClusterRecognition();
		~VFHClusterRecognition();
		
		void setD(int);
		void SetInputDirectory(std::string);
		//void SetInputHistogram2Test(std::string);
		void SetInputHistogram2Test(GlobalDescriptorsPtr);
        void BuildTreeIndex();
        void LoadSavedTreeIndex();
        void ComputeNN();
        void VisualizeResults();
        std::vector<std::string> GetBestClusterResult();
        
    protected:
		 bool LoadHist (const boost::filesystem::path&, vfh_model&);
		 bool LoadHist (GlobalDescriptorsPtr histogramm, vfh_model&);
		 void LoadFeatureModels (const boost::filesystem::path&, const std::string&, std::vector<vfh_model>&);
		 bool LoadFileList(std::vector<vfh_model>&, const std::string&);
		 inline void NearestKSearch (flann::Index<flann::ChiSquareDistance<float> >& , const vfh_model&, 
				int, flann::Matrix<int>&, flann::Matrix<float>&);
		 

    
    private:
		int D;
		double my_clock();
		std::string elapsed(double, double);
		
		std::string inputDir;
		GlobalDescriptorsPtr inputHistogram2Test;		
		std::string extension;
		
		std::string kdtree_idx_file_name;
		std::string training_data_h5_file_name;
		std::string training_data_list_file_name;
		
		std::vector<vfh_model> models;    
		flann::Matrix<float> data;
		std::ofstream fs;		
		flann::Index<flann::ChiSquareDistance<float> >* index;
		
		int nn_k;
		double nn_thresh;
		
		vfh_model histogram2Test;
		
		flann::Matrix<int> nn_k_indices;
		flann::Matrix<float> nn_k_distances;
		
};

#endif
