#ifndef _cad_transformation_h
#define _cad_transformation_h

#include <vtkGenericDataObjectReader.h>
#include <vtkStructuredGrid.h>
#include <vtkSmartPointer.h>
#include <vtkPolyData.h>
#include <vtkIndent.h>
#include <vtkMatrix4x4.h>
#include <string>

#include <iostream>
#include <fstream>

#include <pcl/common/common_headers.h>
#include <pcl/common/common.h>
#include <pcl/common/centroid.h>
#include <pcl/common/transforms.h>
#include <pcl/io/pcd_io.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/console/parse.h>


#include "typedefs.h"

class CAD_Transformation
{
	public:
		CAD_Transformation();
		~CAD_Transformation();
		void setD(int);
		void setPointCloud(PointCloudPtr input);
		void setScene(PointCloudPtr input);
		void setVTKCADModel(std::string);
		void setPoses(std::string);
		void setInitCADPose(std::string);
		void setFinalCADTransformation(Eigen::Matrix4f);
		void setInitCADPoses(std::vector<std::string>);
		void setViewNames(std::vector<std::string>);
		void setFinalCADTransformations(std::vector<Eigen::Matrix4f, Eigen::aligned_allocator< Eigen::Matrix4f > >);
		void applyTransformations();
		void applyAllTransformations();
		void visualizeResults();
		void visualizeAllResults();
		void visualizeBestResult();
		void rankTransformations();
		//void getFinalVTKCADTransform();
                void setFreeSpaceValues(std::vector<double>);
		
		
		
	protected:
		void loadPoses(std::string);
		void loadVTKCADModel(std::string);
		
	private:
		int D;
		double my_clock();
		std::string elapsed(double, double);
				
		PointCloudPtr src_Points;		
		PointCloudPtr scene;		

		std::string vtkCADModel_Path;
		std::string poses_Path;

		int initCADPose;
		std::vector<int> initCADPoses;
		vtkSmartPointer<vtkMatrix4x4>  final_matrix;
		std::vector<vtkSmartPointer<vtkMatrix4x4> >  final_matrices;
		std::vector<Eigen::Matrix4f, Eigen::aligned_allocator< Eigen::Matrix4f > > eigen_matrices;

		std::vector< vtkSmartPointer<vtkMatrix4x4> > poses;
		std::vector<std::string> view_names;

		std::vector<double> fs_values;
		std::vector<int> fs_indices;

		vtkSmartPointer<vtkTransform> transform_view;
		std::vector<vtkSmartPointer<vtkTransform> > transform_views;

		vtkSmartPointer<vtkPolyData> polydata; ///TODO: maybe we have to show multiple cad models
		
		int best_index;
		
		//pcl::visualization::PCLVisualizer vis;	
	
};

#endif
