#ifndef IO_H_
#define IO_H_

#include "typedefs.h"

#include <pcl/io/pcd_io.h>

#define DEBUG 0
class CloudIO
{

public:
  
template <typename PointT>
static boost::shared_ptr<pcl::PointCloud<PointT> >
loadPointCloud (std::string filename, std::string suffix)
{
  boost::shared_ptr<pcl::PointCloud<PointT> > output (new pcl::PointCloud<PointT>);
  filename.append (suffix);
  pcl::io::loadPCDFile (filename, *output);
  if(DEBUG) pcl::console::print_info ("Loaded %s (%zu points)\n", filename.c_str (), output->size ());
  return (output);
}

static PointCloudPtr
loadPoints (std::string filename)
{
  PointCloudPtr output (new PointCloud);
  filename.append ("_points.pcd");
  pcl::io::loadPCDFile (filename, *output);
  if(DEBUG) pcl::console::print_info ("Loaded %s (%zu points)\n", filename.c_str (), output->size ());
  return (output);
}

static SurfaceNormalsPtr
loadSurfaceNormals(std::string filename)
{
  SurfaceNormalsPtr output (new SurfaceNormals);
  filename.append ("_normals.pcd");
  pcl::io::loadPCDFile (filename, *output);
  if(DEBUG) pcl::console::print_info ("Loaded %s (%zu points)\n", filename.c_str (), output->size ());
  return (output);
}

static PointCloudPtr
loadKeypoints (std::string filename)
{
  PointCloudPtr output (new PointCloud);
  filename.append ("_keypoints.pcd");
  pcl::io::loadPCDFile (filename, *output);
  if(DEBUG) pcl::console::print_info ("Loaded %s (%zu points)\n", filename.c_str (), output->size ());
  return (output);
}

static LocalDescriptorsPtr
loadLocalDescriptors (std::string filename)
{
  LocalDescriptorsPtr output (new LocalDescriptors);
  filename.append ("_localdesc.pcd");
  pcl::io::loadPCDFile (filename, *output);
  if(DEBUG) pcl::console::print_info ("Loaded %s (%zu points)\n", filename.c_str (), output->size ());
  return (output);
}

static GlobalDescriptorsPtr
loadGlobalDescriptors (std::string filename)
{
  GlobalDescriptorsPtr output (new GlobalDescriptors);
  filename.append ("_globaldesc.pcd");
  pcl::io::loadPCDFile (filename, *output);
  if(DEBUG) pcl::console::print_info ("Loaded %s (%zu points)\n", filename.c_str (), output->size ());
  return (output);
}

};

#endif
