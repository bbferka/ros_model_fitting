#ifndef _feature_estimation_h
#define _feature_estimation_h

#include <string>
#include "typedefs.h"

#include <pcl/point_types.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/search/kdtree.h>

//typedef pcl::PointXYZRGB PointT;
//typedef pcl::PointXYZRGBNormal PointTNormal;

class FeatureEstimation
{
	public:
		FeatureEstimation();
		~FeatureEstimation();

		void setD(int);
		void setPointCloud(std::string);
		void setPointCloud(pcl::PointCloud<PointT>::Ptr);

		void setSurfaceNormalsRadius(double);
		void setKeypointsRadius(double);
		void setLocalDescriptorsRadius(double);

		void estimateSurfaceNormals();
		void detectKeypoints();
		void computeLocalDescriptors();
		void computeGlobalDescriptor();
		
		void saveAllData(std::string);

		PointCloudPtr getPointCloud();
		SurfaceNormalsPtr getSurfaceNormals();
		PointCloudPtr getKeypoints();
		LocalDescriptorsPtr getLocalDescriptors();
		GlobalDescriptorsPtr getGlobalDescriptor();
		
	private:
		int D;
		double my_clock();
		std::string elapsed(double, double);

		/* Actual point cloud data */
		PointCloudPtr cloud;
		
		/* Surface normals */
		SurfaceNormalsPtr normals;
		double normals_surface_radius;

		/* Harris Keypoints */
		PointCloudPtr keypoints;
		double harris_radius;

		/* Local Descriptors */
		LocalDescriptorsPtr local_descriptors;
		double descriptor_radius;

		/* Global Descriptor */
		GlobalDescriptorsPtr global_descriptor;
};

#endif


