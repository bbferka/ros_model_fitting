#ifndef _freespace_h
#define _freespace_h

#include "typedefs.h"
#include <sys/time.h>
#include <string>

class FreeSpace
{
	public:
		FreeSpace();
		~FreeSpace();

		void setD(int);
		double getScore(PointCloudPtr, PointCloudPtr);
    void setN(int);
		
		
	private:
		//Eigen::Matrix<float,3,4,Eigen::RowMajor> P; // Projection/camera matrix
	  int D;	
	  double my_clock();
	  std::string elapsed(double,double);
	  int N;
		
		void project(double[480][640], PointCloudPtr);
		double compare(double[480][640], double[480][640]);
		void save(double[480][640], std::string);
	
};

#endif
