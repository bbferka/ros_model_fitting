#ifndef _registration_h
#define _registration_h

#include <string>
#include "typedefs.h"

#include <pcl/point_types.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/search/kdtree.h>
#include <pcl/common/centroid.h>
#include <pcl/common/transforms.h>

//typedef pcl::PointXYZRGB PointT;
//typedef pcl::PointXYZRGBNormal PointTNormal;

class Registration
{
	public:
		Registration();
		~Registration();
		///TODO: function to read some/all the parameters from a file...

		void setD(int);
		void setPointClouds(std::string, std::string);
		void setPointClouds(pcl::PointCloud<PointT>::Ptr, pcl::PointCloud<PointT>::Ptr);
		
		void setInitialAlignmentParams(double, double, double);
		void setICPAlignmentParams(double, double, double, double);
		void setTarget(std::string);
		void setSource(std::string);
		void setTarget(PointCloudPtr, PointCloudPtr, LocalDescriptorsPtr);
		void setSource(PointCloudPtr, PointCloudPtr, LocalDescriptorsPtr);
		void setOptions(std::string);
		void computeCentroids3DAndTransform();
		void computeInitialAlignment();
		void refineAlignment();
		void saveFinalPointcloud(std::string);
		PointCloudPtr getFinalPointcloud();

		Eigen::Matrix4f getFinalTransformation();



	private:
		int D;
		double my_clock();
		std::string elapsed(double, double);

		/* Initial Alignment */
		double min_sample_distance, max_correspondence_distance, nr_iterations;

		/* ICP Alignment */
		double max_correspondence_distance_icp, outlier_rejection_threshold, transformation_epsilon, max_iterations;

		/* Actual point cloud data */
		PointCloudPtr src_points;
		PointCloudPtr tgt_points;
		PointCloudPtr src_keypoints;
		PointCloudPtr tgt_keypoints;
		LocalDescriptorsPtr src_descriptors;
		LocalDescriptorsPtr tgt_descriptors;
		Eigen::Matrix4f tform;
};

#endif


