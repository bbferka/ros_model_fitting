#ifndef _segment_from_desk_h
#define _segment_from_desk_h

#include <string>

#include <pcl/point_types.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/search/kdtree.h>


typedef pcl::PointXYZRGB PointT;
typedef pcl::PointXYZRGBNormal PointTNormal;

///TODO: CHANGE STUFF TO typedefs.h
class SegmentFromDesk
{
	public:
		SegmentFromDesk();
		~SegmentFromDesk();
		///TODO: function to read some/all the parameters from a file...

		void setPointCloud(std::string);
		void setPointCloud(pcl::PointCloud<PointT>::Ptr);
		void setParams(double, double);
		void setD(int);

		void execute();
		
		pcl::PointCloud<PointT>::Ptr getInputPointCloud();
		pcl::PointCloud<PointT>::Ptr getSegmentedCloud();
		pcl::PointCloud<PointT>::Ptr getFilteredCloud();
		pcl::PointCloud<pcl::Normal>::Ptr getSegmentedCloudNormals();


	protected:
		void FilterPointCloud();
		void DownsampleFilteredPointCloud();
		void EstimateNormals();
		void DoSegmentation();
		void DoEuclidianClustering();
		void CreateHull();
		void FindObjectInHull();
		

	private:
		int D;
		double my_clock();
		std::string elapsed(double, double);

		/* Parameters for Clustering */
		double x_min, x_max, y_min, y_max, z_min, z_max;
		double nr_neighbors, std_mul, voxel_size, search_radius;
		double angle_weight, eps_angle, inlier_threshold;
		int max_iterations;

		double axis_x, axis_y, axis_z, min_size, conn_threshold;
		double min_height, max_height, smooth_radius, min_plane_size;

		bool select_closest;

		unsigned int max_size;
		int max_cluster;


		/* Actual point cloud data */
		pcl::PointCloud<PointT>::Ptr cloud;
		pcl::PointCloud<PointT>::Ptr cloud_filtered;
		pcl::PointCloud<PointT>::Ptr cloud_downsampled;
		pcl::PointCloud<pcl::Normal>::Ptr cloud_normals;
		pcl::PointCloud<PointT>::Ptr cloud_plane;
		pcl::PointCloud<PointT>::Ptr cloud_hull;
		pcl::ModelCoefficients::Ptr coefficients_plane_best; // (new pcl::ModelCoefficients);
		pcl::PointCloud<PointT>::Ptr mls_points;
		pcl::PointCloud<PointT>::Ptr mls_points_rotated;
		pcl::PointCloud<pcl::Normal>::Ptr mls_points_rotated_normals;

		std::vector<pcl::PointIndices> clusters;
		std::vector<pcl::PointIndices> clusters_final;
		pcl::search::KdTree<PointT>::Ptr tree;
};

#endif
