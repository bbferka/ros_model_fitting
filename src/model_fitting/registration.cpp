#include "model_fitting/registration.h"
//#include "helpers.h"
#include "model_fitting/load_clouds.h"

#include <sys/time.h>

#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>

#include <pcl/registration/ia_ransac.h>
#include <pcl/registration/icp.h>


//#define D 0
#define S 0

//typedef pcl::PointXYZRGB PointT;
//typedef pcl::PointXYZRGBNormal PointTNormal;

double Registration::my_clock()
{
  struct timeval tv;
  gettimeofday(&tv, NULL);
  return tv.tv_sec + (double)tv.tv_usec*1e-6;
}

std::string Registration::elapsed(double start, double end) {
	std::ostringstream os;
	os << (end-start) * 1000 << "ms";
	return os.str();
}


Registration::Registration()
: D(0), src_points(new PointCloud), tgt_points(new PointCloud),
  src_keypoints(new PointCloud), tgt_keypoints(new PointCloud),
  src_descriptors(new LocalDescriptors), tgt_descriptors(new LocalDescriptors),
	tform(Eigen::Matrix4f::Identity())
{
	if(D) std::cout << "[info] Constructor Registration()" << std::endl;
	setInitialAlignmentParams(0.05, 0.02, 500);
	setICPAlignmentParams(0.05, 0.05, 0.0, 100);

}


Registration::~Registration()
{
  if(D) std::cout << "[info] Destructor Registration()" << std::endl;

}

void Registration::setD(int d)
{
	D = d;
}

void Registration::setPointClouds(std::string source, std::string target)
{
	double t1 = my_clock();
	src_points = CloudIO::loadPoints(source); //loadPointCloud<PointT>(source, "_points.pcd");
	tgt_points = CloudIO::loadPoints(target); //loadPointCloud<PointT>(target, "_points.pcd");

	src_keypoints = CloudIO::loadKeypoints(source); //loadPointCloud<PointT>(source, "_keypoints.pcd");
	tgt_keypoints = CloudIO::loadKeypoints(target); //loadPointCloud<PointT>(target, "_keypoints.pcd");
	
	src_descriptors = CloudIO::loadLocalDescriptors(source); //loadPointCloud<LocalDescriptorT>(source, "_localdesc.pcd");
	tgt_descriptors = CloudIO::loadLocalDescriptors(target); //loadPointCloud<LocalDescriptorT>(target, "_localdesc.pcd");

	double t2 = my_clock();
	if(D) std::cout <<  " in " << elapsed(t1,t2) << "." << std::endl;
}


void Registration::setTarget(std::string target)
{
	tgt_points = CloudIO::loadPoints(target);
	tgt_keypoints = CloudIO::loadKeypoints(target);
	tgt_descriptors = CloudIO::loadLocalDescriptors(target);
}


void Registration::setSource(std::string source)
{
	src_points = CloudIO::loadPoints(source);
	src_keypoints = CloudIO::loadKeypoints(source);
	src_descriptors = CloudIO::loadLocalDescriptors(source);
}


void Registration::setTarget(PointCloudPtr cloud,
														 PointCloudPtr keypoints,
														 LocalDescriptorsPtr descriptors)
{
				tgt_points = cloud;
				tgt_keypoints = keypoints;
				tgt_descriptors = descriptors;
}

void Registration::setSource(PointCloudPtr cloud,
														 PointCloudPtr keypoints,
														 LocalDescriptorsPtr descriptors)
{
				src_points = cloud;
				src_keypoints = keypoints;
				src_descriptors = descriptors;
}

void Registration::setPointClouds(pcl::PointCloud<PointT>::Ptr source, pcl::PointCloud<PointT>::Ptr target)
{
	double t1 = my_clock();
	if(D) std::cout << "[info] Setting point cloud..." << std::endl;
	src_points = source;
	tgt_points = target;
	double t2 = my_clock();
	if(D) std::cout << " with " << src_points->points.size() << " and " << tgt_points->points.size()
					<< " points in " << elapsed(t1,t2) << "." << std::endl;
}


void Registration::setInitialAlignmentParams(double min_sample_dist, double max_corresp_dist, double nr_iter)
{
	min_sample_distance = min_sample_dist;
	max_correspondence_distance = max_corresp_dist;
	nr_iterations = nr_iter;
}


void Registration::setICPAlignmentParams(double max_corresp_dist, double outlier_rej_thresh, double eps, double max_iter)
{
	max_correspondence_distance_icp = max_corresp_dist;
	outlier_rejection_threshold = outlier_rej_thresh;
	transformation_epsilon = eps;
	max_iterations = max_iter;
}

void
Registration::computeCentroids3DAndTransform()
{
	double t1 = my_clock();
	if(D) std::cout << "[info] Registration::computeCentroids3DAndTransform()" << std::endl;
	
	Eigen::Vector4f source_centroid;
	Eigen::Vector4f target_centroid;
  
	pcl::compute3DCentroid(*src_points, source_centroid);
	pcl::compute3DCentroid(*tgt_points, target_centroid);
	
	if(D) std::cout << "source_centroid:\n" << source_centroid << std::endl;
	if(D) std::cout << "target_centroid:\n" << target_centroid << std::endl;
	
	Eigen::Vector4f source_to_target_centroid = -source_centroid + target_centroid;
	
	if(D) std::cout << "source_to_target_centroid:\n" << source_to_target_centroid << std::endl;
	if(D) std::cout << "resulting source_centroid:\n" <<  source_centroid + source_to_target_centroid << std::endl;
	
	Eigen::Matrix4f transformation_matrix;
	
	transformation_matrix.setIdentity(4,4);
	
	//std::cout << " Identity:\n" <<  transformation_matrix << std::endl;  
	
	transformation_matrix(0,3)=source_to_target_centroid(0);
	transformation_matrix(1,3)=source_to_target_centroid(1);
	transformation_matrix(2,3)=source_to_target_centroid(2);	

	tform = transformation_matrix;
	double t2 = my_clock();

	if(D) std::cout << "Computed centroid3D transformation matrix:" << std::endl;
	if(D) std::cout << tform << std::endl;

	if(D) std::cout <<  "[info] Registration::computeCentroids3DAndTransform() took " << elapsed(t1,t2) << "." << std::endl;
	
}


void Registration::computeInitialAlignment()
{
	double t1 = my_clock();
	if(D) std::cout << "[info] Registration::computeInitialAlignment(): min_sample_dist=" << min_sample_distance
			<< ", max_corresp_dist=" << max_correspondence_distance << ", nr_iterations="
			<< nr_iterations << std::endl;

	pcl::SampleConsensusInitialAlignment<PointT, PointT, LocalDescriptorT> sac_ia;
	sac_ia.setMinSampleDistance (min_sample_distance);
	sac_ia.setMaxCorrespondenceDistance (max_correspondence_distance);
	sac_ia.setMaximumIterations (nr_iterations);
	sac_ia.setCorrespondenceRandomness(3);
	sac_ia.setInputCloud (src_keypoints);
	sac_ia.setSourceFeatures (src_descriptors);

	sac_ia.setInputTarget (tgt_keypoints);
	sac_ia.setTargetFeatures (tgt_descriptors);

	if(D) std::cout << "[info] Registration::computeInitialAlignment(): starting alignment..." << std::endl;

	PointCloud registration_output;
	sac_ia.align (registration_output);
	tform = sac_ia.getFinalTransformation();
	double t2 = my_clock();

	if(D) std::cout << "Inital alignment matrix:" << std::endl;
	if(D) std::cout << tform << std::endl;

	/*
	return (sac_ia.getFinalTransformation ());
	*/
	if(D) std::cout <<  "[info] Registration::computeInitialAlignment() took " << elapsed(t1,t2) << "." << std::endl;
}

//tform = refineAlignment (src_points, tgt_points, tform, max_correspondence_distance,
                             //outlier_rejection_threshold, transformation_epsilon, max_iterations);

void Registration::refineAlignment()
{
	double t1 = my_clock();
	if(D) std::cout << "[info] ICP: Setting point cloud..." << std::endl;
	if(D) std::cout << "with params: " << max_correspondence_distance_icp << " "
									<< outlier_rejection_threshold << " "
									<< transformation_epsilon << " "
									<< max_iterations;
	pcl::IterativeClosestPoint<PointT, PointT> icp;
	icp.setMaxCorrespondenceDistance (max_correspondence_distance_icp);
	icp.setRANSACOutlierRejectionThreshold (outlier_rejection_threshold);
	icp.setTransformationEpsilon (transformation_epsilon);
	icp.setMaximumIterations (max_iterations);

	PointCloudPtr source_points_transformed (new PointCloud);
	pcl::transformPointCloud (*src_points, *source_points_transformed, tform);

	icp.setInputCloud (source_points_transformed);
	icp.setInputTarget (tgt_points);

	PointCloud registration_output;
	icp.align (registration_output);

	if(D) std::cout << "ICP convergence: " << icp.hasConverged() << " score: " <<
					icp.getFitnessScore() << std::endl;
	if(D) std::cout << icp.getFinalTransformation() << std::endl;

	tform = icp.getFinalTransformation()*tform;
	double t2 = my_clock();

	if(D) std::cout << "Final transformation: " << std::endl;
	if(D) std::cout << tform << std::endl;
	
	if(D) std::cout <<  "[info] Registration::refineAlignment() took " << elapsed(t1,t2) << "." << std::endl;
}


Eigen::Matrix4f Registration::getFinalTransformation()
{
	return tform;
}

void Registration::setOptions(std::string)
{

}

void Registration::saveFinalPointcloud(std::string filename)
{
	PointCloud result;
	pcl::transformPointCloud(*src_points, result, tform);
	pcl::io::savePCDFile (filename, result);
	if(D) std::cout << "[info] Saved transformed pointcloud as " << filename << std::endl;
}

//Eigen::Matrix4f Registration::getFinalTransformation()
//{
	//return NULL;
//}

PointCloudPtr Registration::getFinalPointcloud()
{
	PointCloudPtr result (new PointCloud);
	pcl::transformPointCloud(*src_points, *result, tform);
	return result;
}
