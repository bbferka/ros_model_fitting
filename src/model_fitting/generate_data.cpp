//project specific
#include <model_fitting/vfh_cluster_recognition.h>
#include <model_fitting/registration.h>
#include <model_fitting/feature_estimation.h>

//vtk
#include <vtkGenericDataObjectReader.h>
#include <vtkStructuredGrid.h>
#include <vtkSmartPointer.h>
#include <vtkPolyData.h>

#include <pcl/filters/voxel_grid.h>

//boost
#include <boost/random.hpp>
#include <boost/random/normal_distribution.hpp>
#include <boost/filesystem.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>

//stl
#include <fstream>
#include <iostream>
#include <sys/time.h>
//ros
#include <ros/ros.h>
#include <rospack/rospack.h>
#include <ros/package.h>

namespace fs = boost::filesystem;

#define D 1

typedef pcl::PointCloud<pcl::PointXYZ> Cloud;
typedef pcl::PointCloud<pcl::PointXYZRGB> Cloud_RGB;
typedef pcl::PointCloud<pcl::PointXYZRGB>::Ptr Cloud_RGB_Ptr;

int main(int argc, const char* argv[])
{
	std::cout << "main generate data." << std::endl;

	std::string package_path = ros::package::getPath("model_fitting");
	std::cerr <<package_path<<std::endl;
	boost::property_tree::ptree pt;
	boost::property_tree::ini_parser::read_ini(package_path+"/config_data.ini", pt);
	typedef std::string s;

	std::string inputFilename = pt.get<s>("vtk.cad_model_filename");
	std::string cad_path = pt.get<s>("vtk.cad_model_path");
	int resx = pt.get<int>("vtk.resolution_x"); //128;//atof(argv[2]);
	int resy = pt.get<int>("vtk.resolution_y"); //resx;
	int tesselation_level = pt.get<int>("vtk.tesselation_level"); //3;
	float view_angle = pt.get<int>("vtk.view_angle"); //60;
	float radius_sphere = pt.get<int>("vtk.radius_sphere"); //1;
	bool use_vertices = false;
	double voxel_size = pt.get<double>("downsampling.voxel_size");
	double noise_std = pt.get<double>("noise.std_dev"); //0.005

	vtkSmartPointer<vtkGenericDataObjectReader> reader =
			vtkSmartPointer<vtkGenericDataObjectReader>::New();
	// loop over every cad model in cadmodelfile
	int acc = 0;
	std::vector< Eigen::Matrix4f, Eigen::aligned_allocator< Eigen::Matrix4f > > all_poses;
	std::vector<std::string> filenames;
	std::ifstream infile(inputFilename.c_str());
	if(infile.is_open())
	{
		while(infile.good())
		{
			std::string line;
			getline(infile, line);
			filenames.push_back(line);
		}
	}

	for(size_t f = 0; f < filenames.size() - 1; ++f)
	{
		std::string file = cad_path;
		std::cerr<<"PATH TO CAD: "<<file<<std::endl;
		file.append(filenames[f]);
		if (D) std::cout << "vtk file string: " << std::endl;
		reader->SetFileName(file.c_str());
		reader->Update();

		vtkPolyData* polydata = reader->GetPolyDataOutput();
		if(D) std::cout << "[info] model has " << polydata->GetNumberOfPoints() << " points." << std::endl;

		pcl::visualization::PCLVisualizer vis("Visualizer");
		vis.addModelFromPolyData (polydata, "mesh1", 0);
		vis.setRepresentationToSurfaceForAllActors();


		std::vector< Cloud, Eigen::aligned_allocator< pcl::PointCloud< pcl::PointXYZ > > >  views_xyz;
		std::vector< Eigen::Matrix4f, Eigen::aligned_allocator< Eigen::Matrix4f > > poses;
		std::vector<float> entropies;


		vis.renderViewTesselatedSphere (resx, resy, views_xyz, poses, entropies,
				tesselation_level, view_angle, radius_sphere, use_vertices);

		if(D) std::cout << "[info] nr of views generated: " << views_xyz.size() << std::endl;

		// put all poses at the end of all_poses
		all_poses.insert(all_poses.end(), poses.begin(), poses.end());


		int i;
		std::string views_dir = pt.get<s>("vtk.views_dir");
		///TODO: check if folder data exists and if not create it
		//fs::path cwd = fs::current_path();
		//cwd /= "..";
		//cwd /= views_dir;
		//if(!fs::is_directory(cwd)) fs::create_directory(cwd);
		//cwd /= pt.get<s>("vtk.views_name"); //"../views/syn_cad_view_";

		//std::string str_start = cwd.string();
		std::string str_start = views_dir;
		str_start.append("/");
		str_start.append( filenames[f].substr(0,7)); // use first 8 characters of vtk file //pt.get<s>("vtk.views_name");
		str_start.append("_");
		std::string str_end = ".pcd";

		struct timeval start;
		gettimeofday (&start, NULL);
		boost::mt19937 rng;
		rng.seed (start.tv_usec);
		boost::normal_distribution<> nd (0.0, noise_std);
		boost::variate_generator<boost::mt19937&, boost::normal_distribution<> > var_nor (rng, nd);
		for(i=0; i<views_xyz.size(); i++)
		{
			std::string output;
			output.append(str_start);
			output.append(boost::lexical_cast<std::string>( acc ));
			acc = acc + 1; // fortlaufende zahl zur identifikation
			//output.append(str_end);

			Cloud_RGB_Ptr cloud_rgb (new Cloud_RGB);
			pcl::copyPointCloud(views_xyz[i], *cloud_rgb);
			//pcl::io::savePCDFileBinary(output, cloud_rgb);
			Cloud_RGB_Ptr cloud_downsampled (new Cloud_RGB);
			pcl::VoxelGrid<PointT> vg;
			vg.setInputCloud (cloud_rgb);
			vg.setLeafSize (voxel_size, voxel_size, voxel_size);
			vg.filter (*cloud_downsampled);

			// Noisify each point in the dataset
			for (size_t cp = 0; cp < cloud_downsampled->points.size (); ++cp)
			{
				cloud_downsampled->points[cp].z += var_nor ();
			}

			FeatureEstimation feat;
			feat.setPointCloud(cloud_downsampled); //pt.get<s>("feature.cloud"));
			feat.setSurfaceNormalsRadius(pt.get<double>("feature.normals_radius"));
			feat.setKeypointsRadius(pt.get<double>("feature.keypoints_radius"));
			feat.setLocalDescriptorsRadius(pt.get<double>("feature.local_descriptors_radius"));

			feat.estimateSurfaceNormals();
			feat.detectKeypoints();
			feat.computeLocalDescriptors();
			feat.computeGlobalDescriptor();
			feat.saveAllData(output);

			//pcl::io::savePCDFileBinary(output, *cloud_downsampled);
		}
	}
	std::ofstream out_file;
	out_file.open(pt.get<s>("vtk.poses_filename").c_str());
	out_file << all_poses.size() << std::endl;
	for(size_t i=0; i < acc; ++i)
	{
		out_file << all_poses[i] << std::endl;
	}
	out_file.close();

	std::cout << "done." << std::endl;
	return 0;
}
