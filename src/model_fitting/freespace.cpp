#include "model_fitting/freespace.h"
#include <fstream>
#include <boost/math/special_functions/round.hpp>
#include <boost/lexical_cast.hpp>
#include "model_fitting/typedefs.h"

//#define D 0
#define S 0

    double 
FreeSpace::my_clock()
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return tv.tv_sec + (double)tv.tv_usec*1e-6;
}

std::string 
FreeSpace::elapsed(double start, double end) {
    std::ostringstream os;
    os << (end-start) * 1000 << "ms";
    return os.str();
}

FreeSpace::FreeSpace()
: D(0), N(0)
{
    //P << 525.0, 0.0, 319.5, 0.0, 0.0, 525.0, 239.5, 0.0, 0.0, 0.0, 1.0, 0.0;
    if(D) std::cout << "[info] Constructor FreeSpace()" << std::endl;
    //if(D) std::cout << P << std::endl;
}

FreeSpace::~FreeSpace()
{
    if(D) std::cout << "[info] Destructor FreeSpace()" << std::endl;

}

void FreeSpace::setD(int d)
{
	D = d;
}

void FreeSpace::setN(int n)
{
    N = n;
}

double FreeSpace::getScore(PointCloudPtr sceneCloud, PointCloudPtr cadmodelCloud)
{
    if(D) std::cout << "[info] Getting freespace score..." << std::endl;
    double scene[480][640];
    double cadmodel[480][640];
    project(scene, sceneCloud);
    project(cadmodel, cadmodelCloud);
    if(S) save(scene, "scene");
    if(S) save(cadmodel, "cadmodel");
    double score = compare(scene, cadmodel);

    return score;
}


void FreeSpace::save(double image[480][640], std::string name)
{
    if(D) std::cout << "[info] Saving " << name << "..." << std::endl;
    name.append(boost::lexical_cast<std::string>(N));
    name.append(".pgm");
    std::ofstream ofile;
    ofile.open(name.c_str());

    ofile << "P2\n640 480\n150\n";
    for(size_t r = 0; r < 480; ++r)
    {
        for(size_t c = 0; c < 640; ++c)
        {
            ofile << int(image[r][c] * 100) << " ";
        }
        ofile << std::endl;
    }
    ofile.close();
}


double FreeSpace::compare(double scene[480][640], double cadmodel[480][640])
{
    if(D) std::cout << "[info] Comparing scenes..." << std::endl;
    //double result[480][640];
    double explained_threshold = 0.01;
    double explained_mult = 2.0;
    size_t score_acc = 0;
    size_t explained_points = 0, unexplained_points = 0, occluded_points = 0;
    //double ep[480][640];
    //double result_up[480][640];
    //double op[480][640];

    for(size_t r = 0; r < 480; ++r)
    {
        for(size_t c = 0; c < 640; ++c)
        {
            //ep[r][c] = 0;
            //result_up[r][c] = 0;
            //op[r][c] = 0;
            // find explained points
            if( cadmodel[r][c] != 0)
            {
                // if squared distance below threshold, point is explained
                double sd = fabs(cadmodel[r][c] - scene[r][c]);
                if (sd < explained_threshold)
                {
                    //std::cout << sd << " ";
                    ++explained_points;
                    //ep[r][c] = 1;
                }
                else if( scene[r][c] > cadmodel[r][c]) // unexplained points
                {
                    ++unexplained_points;
                    //result_up[r][c] = 1;
                }
                else if( scene[r][c] < cadmodel[r][c])
                {
                    ++occluded_points;
                    //op[r][c] = 1;
                }
                else
                {
                    std::cout << "unknown case!" << std::endl;
                }
            }
            // find unexplained points
            // find occluded points

            /*
            result[r][c] = 0;
            double diff = scene[r][c] - cadmodel[r][c]; 
            //if((cadmodel[r][c] != 0) && cadmodel[r][c] < scene[r][c])
            //if((cadmodel[r][c] != 0) && diff > t)
            if((cadmodel[r][c] != 0) && cadmodel[r][c] < scene[r][c] && diff > t)
            {
                std::cout << diff << " ";
                //if(abs(image1[r][c] - image2[r][c]) > t)
                result[r][c] = 1;
                ++score_acc;
            }
            */
        }
    }
    std::cout << "\nScore: " << explained_points << " " << unexplained_points << " " << occluded_points << std::endl;

    //save(result, "res");
    //save(ep, "ep");
    //save(result_up, "up");
    //save(op, "op");

    double total = explained_points*explained_mult + unexplained_points + occluded_points;
    return (double)(explained_points*explained_mult + occluded_points ) / (double)total;

}


void FreeSpace::project(double image[480][640], PointCloudPtr cloud)
{
    if(D) std::cout << "[info] Projecting point cloud..." << std::endl;

    //Eigen::Matrix<float,3,3,Eigen::RowMajor> K; // Intrinsic camera matrix for the raw (distorted) images.
    //K << 525.0, 0.0, 319.5, 0.0, 525.0, 239.5, 0.0, 0.0, 1.0;

    Eigen::Matrix<float,3,4,Eigen::RowMajor> P; // Projection/camera matrix
    P << 525.0, 0.0, 319.5, 0.0, 0.0, 525.0, 239.5, 0.0, 0.0, 0.0, 1.0, 0.0;

    //double image[480][640];
    for(size_t r = 0; r < 480; ++r)
        for(size_t c = 0; c < 640; ++c)
            image[r][c] = 0;

    for(size_t i = 0; i < cloud->points.size(); ++i)
    {
        double x, y, z;
        x = cloud->points[i].data[0];
        y = cloud->points[i].data[1];
        z = cloud->points[i].data[2];
        Eigen::Matrix<float,1,4,Eigen::RowMajor> p;
        p << x, y, z, 1;

        Eigen::Matrix<float,3,1> r;
        r = P * p.transpose();

        int xi, yi;
        xi = boost::math::iround(r(0)/r(2));
        yi = boost::math::iround(r(1)/r(2));
        //std::cout << xi << "|" << yi << ": " << z << std::endl;
        image[yi][xi] = z; ///TODO: if two points fall into the same grid cell, take the nearer(?) one
    }
}
