
#include <iostream>
#include "model_fitting/segment_from_desk.h"
#include "model_fitting/vfh_cluster_recognition.h"
#include "model_fitting/registration.h"
#include "model_fitting/feature_estimation.h"
#include "model_fitting/cad_transformation.h"
#include "model_fitting/freespace.h"
#include <sys/time.h>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>

#include <ros/ros.h>
#include <rospack/rospack.h>
#include <ros/package.h>

double 
my_clock()
{
	struct timeval tv;
	gettimeofday(&tv, NULL);
	return tv.tv_sec + (double)tv.tv_usec*1e-6;
}

std::string 
elapsed(double start, double end) {
	std::ostringstream os;
	os << (end-start) * 1000 << "ms";
	return os.str();
}

int main(int argc, const char* argv[])
{
	std::cout << "main." << std::endl;
	double t1 = my_clock();
	std::string package_path = ros::package::getPath("model_fitting");

	boost::property_tree::ptree pt;
	boost::property_tree::ini_parser::read_ini(package_path+"/config.ini", pt);
	boost::property_tree::ptree pt_data;
	boost::property_tree::ini_parser::read_ini(package_path+"/config_data.ini", pt_data);
	typedef std::string s;
	int D = pt.get<int>("main.D");

	SegmentFromDesk seg;
	seg.setD(D);
	seg.setPointCloud(pt.get<s>("clustering.cloud"));
	seg.setParams(pt.get<double>("clustering.voxel_size"), pt.get<double>("clustering.std_mul"));
	seg.execute();

	FeatureEstimation feat;
	feat.setD(D);
	feat.setPointCloud(seg.getSegmentedCloud());
	feat.setSurfaceNormalsRadius(pt_data.get<double>("feature.normals_radius"));
	feat.setKeypointsRadius(pt_data.get<double>("feature.keypoints_radius"));
	feat.setLocalDescriptorsRadius(pt_data.get<double>("feature.local_descriptors_radius"));
	feat.estimateSurfaceNormals();
	feat.detectKeypoints();
	feat.computeLocalDescriptors();
	feat.computeGlobalDescriptor();
	feat.saveAllData(pt_data.get<s>("feature.result_file"));

	VFHClusterRecognition vfhCR;
	vfhCR.setD(D);
	vfhCR.SetInputDirectory(pt.get<s>("vfh.input_dir"));
	// TODO: this must be the result from above!
	vfhCR.SetInputHistogram2Test(feat.getGlobalDescriptor());
	if(pt.get<int>("main.build_tree"))
	{
		vfhCR.BuildTreeIndex();
	}
	else
	{
		std::cout << "====================================================================================" << std::endl;
		std::cout << "====================================================================================" << std::endl;
		std::cout << "[WARNING] [WARNING] [WARNING] NOT BUILDING THE FLANN TREE, JUST LOADING IT!!!!!!!!!!" << std::endl;
		std::cout << "====================================================================================" << std::endl;
		std::cout << "====================================================================================" << std::endl;
	}
	vfhCR.LoadSavedTreeIndex();
	vfhCR.ComputeNN();
	//vfhCR.VisualizeResults();
	int mi = pt.get<int>("main.model_index");
	std::cout << "Best Cluster Result found for: " << vfhCR.GetBestClusterResult()[mi] << std::endl;
	//std::string best_cluster = vfhCR.GetBestClusterResult()[mi];

	// PARALLELIZATION: use n best partial views from here on
	int particles = pt.get<int>("main.particles");
	std::vector<std::string> best_clusters = vfhCR.GetBestClusterResult();
	std::vector<Eigen::Matrix4f, Eigen::aligned_allocator< Eigen::Matrix4f > > final_transformations;
	std::vector<double> fs_values;
	for(size_t p = 0; p < particles; ++p)
	{
		std::string best_cluster = vfhCR.GetBestClusterResult()[p];
		Registration reg;
		reg.setD(D);
		// source: synthetic cad model; target: extracted cluster
		reg.setTarget(feat.getPointCloud(), feat.getKeypoints(), feat.getLocalDescriptors()); // toaster
		reg.setSource(vfhCR.GetBestClusterResult()[p]); // cad model
		//reg.setTarget(pt.get<s>("registration.target"));
		//reg.setSource(pt.get<s>("registration.source"));
		//reg.setPointClouds(pt.get<s>("registration.source"), pt.get<s>("registration.target"));
		reg.setInitialAlignmentParams(pt.get<double>("registration.init_min_sample_dist"),
				pt.get<double>("registration.init_max_correspondence_dist"),
				pt.get<int>   ("registration.init_max_iterations"));
		reg.setICPAlignmentParams(pt.get<double>("registration.icp_max_correspondence_dist"),
				pt.get<double>("registration.icp_outlier_rejection_threshold"),
				pt.get<double>("registration.icp_transformation_epsilon"),
				pt.get<int>   ("registration.icp_max_iterations"));
		reg.computeInitialAlignment();
		//reg.computeCentroids3DAndTransform();
		reg.refineAlignment();
		reg.saveFinalPointcloud(pt.get<s>("registration.save_filename"));
		final_transformations.push_back(reg.getFinalTransformation());

		FreeSpace fs;
		fs.setD(D);
		fs.setN(p);
		double fs_score = fs.getScore(seg.getFilteredCloud(),reg.getFinalPointcloud());
		//fs.getScore(seg.getFilteredCloud(), reg.getFinalPointcloud());
		// or getSegmentedCloud() (only the segmented model)
		fs_values.push_back(fs_score);
		std::cout << "[INFO] FREESPACE-SCORE: " << fs_score << std::endl;
	}


	double t2 = my_clock();
	std::cout << "processed " << pt.get<s>("clustering.cloud") << std::endl;
	std::cout << "main() done in: " << elapsed(t1,t2) << "." << std::endl;
	std::cout << "Starting rendering process now." << std::endl;

	CAD_Transformation cad;
	cad.setD(D);
	cad.setPointCloud(seg.getSegmentedCloud());
	//cad.setPointCloud(seg.getInputPointCloud());
	std::string cad_model_name = pt_data.get<s>("vtk.cad_model_path");
	cad_model_name.append(best_clusters[0].substr(9,7)); // only takes first cad model!!
	cad_model_name.append(".vtk");
	cad.setVTKCADModel(cad_model_name);
	cad.setPoses(pt_data.get<s>("vtk.poses_filename"));
	cad.setInitCADPoses(best_clusters);
	cad.setViewNames(best_clusters);
	cad.setFreeSpaceValues(fs_values);
	//cad.setInitCADPose(vfhCR.GetBestClusterResult()[p]);
	//cad.setFinalCADTransformation(reg.getFinalTransformation());
	cad.setFinalCADTransformations(final_transformations);
	//cad.applyTransformations();
	cad.applyAllTransformations();
	cad.rankTransformations();
	//cad.visualizeResults();
	cad.visualizeAllResults();
	cad.setScene(seg.getInputPointCloud());
	cad.visualizeBestResult();
	return 0;
}
