#include "model_fitting/vfh_cluster_recognition.h"

//#define D 1
#define S 0

double 
VFHClusterRecognition::my_clock()
{
  struct timeval tv;
  gettimeofday(&tv, NULL);
  return tv.tv_sec + (double)tv.tv_usec*1e-6;
}

std::string 
VFHClusterRecognition::elapsed(double start, double end) {
	std::ostringstream os;
	os << (end-start) * 1000 << "ms";
	return os.str();
}


VFHClusterRecognition::VFHClusterRecognition() 
: D(0), extension (".pcd"), kdtree_idx_file_name ("../kdtree.idx"), training_data_h5_file_name  ("../training_data.h5"), training_data_list_file_name ("../training_data.list"),
nn_k (6), nn_thresh (DBL_MAX)
{
	transform (extension.begin (), extension.end (), extension.begin (), (int(*)(int))tolower);
	
	if(D) std::cout << "[info] Constructor VFHClusterRecognition()" << std::endl;
}


VFHClusterRecognition::~VFHClusterRecognition()
{
	if(D) std::cout << "[info] Destructor VFHClusterRecognition()" << std::endl;
}

void VFHClusterRecognition::setD(int d)
{
	D = d;
}

void
VFHClusterRecognition::SetInputDirectory(std::string path)
{
	inputDir = path;
	if(D) std::cout << "[info] inputDirectory:  " << inputDir << std::endl;
}

void
VFHClusterRecognition::SetInputHistogram2Test(GlobalDescriptorsPtr histogram)
{
	inputHistogram2Test = histogram;
	//if(D) std::cout << " inputHistogram2Test:  " << inputHistogram2Test << std::endl;
}


bool
VFHClusterRecognition::LoadHist (const boost::filesystem::path &path, vfh_model &vfh)
{
	
    int vfh_idx;
    // Load the file as a PCD
    try
    {
        sensor_msgs::PointCloud2 cloud;
        int version;
        Eigen::Vector4f origin;
        Eigen::Quaternionf orientation;
        pcl::PCDReader r;
        int type; unsigned int idx;
        r.readHeader (path.string (), cloud, origin, orientation, version, type, idx,0);
        vfh_idx = pcl::getFieldIndex (cloud, "vfh");
        if (vfh_idx == -1)
            return (false);
		if ((int)cloud.width * cloud.height != 1)
			return (false);
			
    }
    catch (pcl::InvalidConversionException e)
    {
		if(D) std::cout << " InvalidConversionException:  " << inputHistogram2Test << std::endl;
        return (false);
    }

    // Treat the VFH signature as a single Point Cloud
    pcl::PointCloud <pcl::VFHSignature308> point;
    pcl::io::loadPCDFile (path.string (), point);
    vfh.second.resize (308);

    std::vector <sensor_msgs::PointField> fields;
    pcl::getFieldIndex (point, "vfh", fields);

    for (size_t i = 0; i < fields[vfh_idx].count; ++i)
    {
        vfh.second[i] = point.points[0].histogram[i];
    }
    vfh.first = path.string ();
    return (true);
}

void
VFHClusterRecognition::LoadFeatureModels (const boost::filesystem::path &base_dir, const std::string &extension, 
        std::vector<vfh_model> &models)
{	
    if (!boost::filesystem::exists (base_dir) && !boost::filesystem::is_directory (base_dir))
        return;   

    for (boost::filesystem::directory_iterator it (base_dir); it != boost::filesystem::directory_iterator (); ++it)
    {
        if (boost::filesystem::is_directory (it->status ()))
        {
            std::stringstream ss;
            ss << it->path ();
            pcl::console::print_highlight ("Loading %s (%lu models loaded so far).\n", ss.str ().c_str (), (unsigned long)models.size ());
            LoadFeatureModels (it->path (), extension, models);
        }
        if (boost::filesystem::is_regular_file (it->status ()) && boost::filesystem::extension (it->path ()) == extension)
        {
            vfh_model m;
            if (LoadHist (base_dir / it->path ().filename (), m))
                models.push_back (m);
        }
    }
}


void
VFHClusterRecognition::BuildTreeIndex()
{
	double t1 = my_clock();
	
	LoadFeatureModels (inputDir, extension, models);
	pcl::console::print_highlight ("[info] Loaded %d VFH models. Creating training data %s/%s. ", 
		(int)models.size (), training_data_h5_file_name.c_str (), training_data_list_file_name.c_str ());
	
	
	
	data = flann::Matrix<float>(new float[models.size () * models[0].second.size ()], models.size (), models[0].second.size ());
	
	for (size_t i = 0; i < data.rows; ++i)
        for (size_t j = 0; j < data.cols; ++j)
            data[i][j] = models[i].second[j];

    // Save data to disk (list of models)
    flann::save_to_file (data, training_data_h5_file_name, "training_data");
    
    double t2= my_clock();
	if(D) std::cout <<  "in " << elapsed(t1,t2) << "." << std::endl;
    
    
    double t3 = my_clock();
    fs.open (training_data_list_file_name.c_str ());
    for (size_t i = 0; i < models.size (); ++i)
        fs << models[i].first << "\n";
    fs.close ();

    // Build the tree index and save it to disk
    pcl::console::print_highlight ("[info] Building the kdtree index (%s) for %d elements... ", kdtree_idx_file_name.c_str (), (int)data.rows);

    index = new flann::Index<flann::ChiSquareDistance<float> > (data, flann::LinearIndexParams());
    //index = new flann::Index<flann::ChiSquareDistance<float> > (data, flann::KDTreeIndexParams (4)); //true KDtree, faster approx than LinearIndexParams
    index->buildIndex ();
    index->save (kdtree_idx_file_name);
    //delete[] data.ptr ();
    
    double t4 = my_clock();
	if(D) std::cout <<  "took " << elapsed(t3,t4) << "." << std::endl;
	

}

void
VFHClusterRecognition::LoadSavedTreeIndex()
{
	double t1 = my_clock();
	
	 // Check if the data has already been saved to disk
    if (!boost::filesystem::exists (training_data_h5_file_name) || !boost::filesystem::exists (training_data_list_file_name))
    {
        pcl::console::print_error ("Could not find training data models files %s and %s!\n", 
                training_data_h5_file_name.c_str (), training_data_list_file_name.c_str ());
        return;
    }
    else
    {
		double t1 = my_clock();
		
        LoadFileList (models, training_data_list_file_name);
        flann::load_from_file (data, training_data_h5_file_name, "training_data");
        if(D) pcl::console::print_highlight ("[info] Training data found. Loaded %d VFH models from %s/%s. ", 
                (int)data.rows, training_data_h5_file_name.c_str (), training_data_list_file_name.c_str ());
                
        double t2 = my_clock();
		if(D) std::cout <<  "in " << elapsed(t1,t2) << "." << std::endl;  
    }

    // Check if the tree index has already been saved to disk
    if (!boost::filesystem::exists (kdtree_idx_file_name))
    {
        pcl::console::print_error ("Could not find kd-tree index in file %s!", kdtree_idx_file_name.c_str ());
        return;
    }
    else
    {
		double t3 = my_clock();
		
        index = new flann::Index<flann::ChiSquareDistance<float> > (data, flann::SavedIndexParams (kdtree_idx_file_name));
        index->buildIndex ();
        if(D) pcl::console::print_highlight ("[info] Loaded and built kd-tree index in file %s! ", kdtree_idx_file_name.c_str ());
        
        double t4 = my_clock();
		if(D) std::cout <<  "in " << elapsed(t3,t4) << "." << std::endl;  
    }
    
      
}

bool 
VFHClusterRecognition::LoadFileList(std::vector<vfh_model> &models, const std::string &filename)
{
	ifstream fs;
    fs.open (filename.c_str ());
    if (!fs.is_open () || fs.fail ())
        return (false);

    std::string line;
    while (!fs.eof ())
    {
        getline (fs, line);
        if (line.empty ())
            continue;
        vfh_model m;
        m.first = line;
        models.push_back (m);
    }
    fs.close ();
    return (true);
	
}

inline void
VFHClusterRecognition::NearestKSearch (flann::Index<flann::ChiSquareDistance<float> > &index, const vfh_model &model, 
        int k, flann::Matrix<int> &indices, flann::Matrix<float> &distances)
{
	double t1 = my_clock();
	
	// Query point
    flann::Matrix<float> p = flann::Matrix<float>(new float[model.second.size ()], 1, model.second.size ());
    memcpy (&p.ptr ()[0], &model.second[0], p.cols * p.rows * sizeof (float));

    indices = flann::Matrix<int>(new int[k], 1, k);
    distances = flann::Matrix<float>(new float[k], 1, k);
    index.knnSearch (p, indices, distances, k, flann::SearchParams (512));  
    delete[] p.ptr ();
    
    double t2 = my_clock();
    if(D) std::cout <<  "[info] NearestKSearch () took: " << elapsed(t1,t2) << "." << std::endl; 
    
}

void 
VFHClusterRecognition::ComputeNN()
{
	// Load the test histogram
	if (!LoadHist (inputHistogram2Test, histogram2Test))
    {
        pcl::console::print_error ("Input PointCloud is of wrong type \n");
        return;
    } 

    NearestKSearch (*index, histogram2Test, nn_k, nn_k_indices, nn_k_distances);
    
    // Output the results on screen
    if(D) pcl::console::print_highlight ("The closest %d neighbors for the cluster are:\n", nn_k);
    for (int i = 0; i < nn_k; ++i)
        if(D) pcl::console::print_info ("    %d - %s (%d) with a distance of: %f\n", 
                i, models.at (nn_k_indices[0][i]).first.c_str (), nn_k_indices[0][i], nn_k_distances[0][i]);
                
}

void 
VFHClusterRecognition::VisualizeResults()
{
	// Load the results
    pcl::visualization::PCLVisualizer p ("VFH Cluster Classifier");
    int y_s = (int)floor (sqrt ((double)nn_k));
    int x_s = y_s + (int)ceil ((nn_k / (double)y_s) - y_s);
    double x_step = (double)(1 / (double)x_s);
    double y_step = (double)(1 / (double)y_s);
    pcl::console::print_highlight ("Preparing to load "); 
    pcl::console::print_value ("%d", nn_k); 
    pcl::console::print_info (" files ("); 
    pcl::console::print_value ("%d", x_s);    
    pcl::console::print_info ("x"); 
    pcl::console::print_value ("%d", y_s); 
    pcl::console::print_info (" / ");
    pcl::console::print_value ("%f", x_step); 
    pcl::console::print_info ("x"); 
    pcl::console::print_value ("%f", y_step); 
    pcl::console::print_info (")\n");

    int viewport = 0, l = 0, m = 1;
    for (int i = 0; i < nn_k; ++i)
    {
        std::string cloud_name = models.at (nn_k_indices[0][i]).first;
        boost::replace_last (cloud_name, "_vfh", "");

        p.createViewPort (l * x_step, m * y_step, (l + 1) * x_step, (m + 1) * y_step, viewport);
        l++;
        if (l >= x_s)
        {
            l = 0;
            m--;
        }

        sensor_msgs::PointCloud2 cloud;
        pcl::console::print_highlight (stderr, "Loading "); pcl::console::print_value (stderr, "%s ", cloud_name.c_str ());
        if (pcl::io::loadPCDFile (cloud_name, cloud) == -1)
            break;

        // Convert from blob to PointCloud
        pcl::PointCloud<pcl::PointXYZ> cloud_xyz;
        pcl::fromROSMsg (cloud, cloud_xyz);

        if (cloud_xyz.points.size () == 0)
            break;

        pcl::console::print_info ("[done, "); 
        pcl::console::print_value ("%d", (int)cloud_xyz.points.size ()); 
        pcl::console::print_info (" points]\n");
        pcl::console::print_info ("Available dimensions: "); 
        pcl::console::print_value ("%s\n", pcl::getFieldsList (cloud).c_str ());

        // Demean the cloud
        Eigen::Vector4f centroid;
        pcl::compute3DCentroid (cloud_xyz, centroid);
        pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_xyz_demean (new pcl::PointCloud<pcl::PointXYZ>);
        pcl::demeanPointCloud<pcl::PointXYZ> (cloud_xyz, centroid, *cloud_xyz_demean);
        // Add to renderer*
        p.addPointCloud (cloud_xyz_demean, cloud_name, viewport);

        // Check if the model found is within our inlier tolerance
        std::stringstream ss;
        ss << nn_k_distances[0][i];
        if (nn_k_distances[0][i] > nn_thresh)
        {
            p.addText (ss.str (), 20, 30, 1, 0, 0, ss.str (), viewport);  // display the text with red

            // Create a red line
            pcl::PointXYZ min_p, max_p;
            pcl::getMinMax3D (*cloud_xyz_demean, min_p, max_p);
            std::stringstream line_name;
            line_name << "line_" << i;
            p.addLine (min_p, max_p, 1, 0, 0, line_name.str (), viewport);
            p.setShapeRenderingProperties (pcl::visualization::PCL_VISUALIZER_LINE_WIDTH, 5, line_name.str (), viewport);
        }
        else
            p.addText (ss.str (), 20, 30, 0, 1, 0, ss.str (), viewport);

        // Increase the font size for the score*
        p.setShapeRenderingProperties (pcl::visualization::PCL_VISUALIZER_FONT_SIZE, 18, ss.str (), viewport);

        // Add the cluster name
        p.addText (cloud_name, 20, 10, cloud_name, viewport);
    }
    // Add coordianate systems to all viewports
    p.addCoordinateSystem (0.1, 0);

    p.spin ();
}

std::vector<std::string> 
VFHClusterRecognition::GetBestClusterResult()
{
	std::vector<std::string> result_paths;
		
	for (int i = 0; i < nn_k; ++i)
	{
       std::string temp = models.at (nn_k_indices[0][i]).first.c_str ();
       
       temp.erase(temp.length()-13,13);
       
       result_paths.push_back (temp);  
	}               
	
	return  result_paths;
}

bool
VFHClusterRecognition::LoadHist(GlobalDescriptorsPtr point, vfh_model& vfh)
{
	if(D) std::cout << "[info] Using pointer to point cloud" << std::endl;
	
	// Treat the VFH signature as a single Point Cloud
    vfh.second.resize (308);

    std::vector <sensor_msgs::PointField> fields;
    pcl::getFieldIndex (*point, "vfh", fields);

    for (size_t i = 0; i < fields[0].count; ++i)
    {
        vfh.second[i] = point->points[0].histogram[i];
    }
    vfh.first = "clusteredPointCloud";
    return true;
		
}



