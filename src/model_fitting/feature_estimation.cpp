#include "model_fitting/feature_estimation.h"
#include "model_fitting/load_clouds.h"

#include <sys/time.h>

#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>

#include <pcl/features/normal_3d.h>
#include <pcl/features/normal_3d_omp.h>

#include <pcl/keypoints/harris_keypoint3D.h>

#include <pcl/features/fpfh.h>
#include <pcl/features/fpfh_omp.h>

#include <pcl/features/vfh.h>



//#define D 0
#define S 0
#define OMP

double FeatureEstimation::my_clock()
{
	struct timeval tv;
	gettimeofday(&tv, NULL);
	return tv.tv_sec + (double)tv.tv_usec*1e-6;
}

std::string FeatureEstimation::elapsed(double start, double end) {
	std::ostringstream os;
	os << (end-start) * 1000 << "ms";
	return os.str();
}


FeatureEstimation::FeatureEstimation()
: D(0), cloud(new PointCloud), normals(new SurfaceNormals), normals_surface_radius(0.05), harris_radius(0.04),
  keypoints(new PointCloud), local_descriptors(new LocalDescriptors), descriptor_radius(0.03),
  global_descriptor(new GlobalDescriptors)
{
	if(D) std::cout << "[info] Constructor FeatureEstimation()" << std::endl;
}


FeatureEstimation::~FeatureEstimation()
{
	if(D) std::cout << "[info] Destructor FeatureEstimation()" << std::endl;
}

void FeatureEstimation::setD(int d)
{
	D = d;
}

void FeatureEstimation::setPointCloud(std::string name)
{
	double t1 = my_clock();
	cloud = CloudIO::loadPoints(name); //loadPointCloud<PointT>(source, "_points.pcd");
	double t2 = my_clock();
	if(D) std::cout <<  "in " << elapsed(t1,t2) << "." << std::endl;
}


void FeatureEstimation::setPointCloud(pcl::PointCloud<PointT>::Ptr input)
{
	cloud = input;
}


void FeatureEstimation::estimateSurfaceNormals()
{
	if(D) std::cout << "[info] FeatureEstimation::estimateSurfaceNormals(): radius = "
			<< normals_surface_radius << std::endl;
	double t1 = my_clock();
#ifdef OMP
	pcl::NormalEstimationOMP<PointT, NormalT> normal_estimation;
#else
	pcl::NormalEstimation<PointT, NormalT> normal_estimation;
#endif
	normal_estimation.setSearchMethod (pcl::search::KdTree<PointT>::Ptr (new pcl::search::KdTree<PointT>));
	normal_estimation.setRadiusSearch (normals_surface_radius);
	normal_estimation.setInputCloud (cloud);
	normal_estimation.compute (*normals);
	double t2 = my_clock();
	if(D) std::cout << "[info] Calculated " << normals->size() << " normals" << std::endl;
	if(D) std::cout << "[info] FeatureEstimation::estimateSurfaceNormals() took " << elapsed(t1,t2) << std::endl;
}


void FeatureEstimation::detectKeypoints()
{
	if(D) std::cout << "[info] FeatureEstimation::detectKeypoints(): radius = "
			<< harris_radius << std::endl;
	double t1 = my_clock();
	pcl::HarrisKeypoint3D<pcl::PointXYZRGB,pcl::PointXYZI> harris3D_detect;
	harris3D_detect.setSearchMethod (pcl::search::KdTree<PointT>::Ptr (new pcl::search::KdTree<PointT>));
	harris3D_detect.setNonMaxSupression(true);
	harris3D_detect.setRadiusSearch(harris_radius);
	harris3D_detect.setMethod(pcl::HarrisKeypoint3D<pcl::PointXYZRGB,pcl::PointXYZI>::HARRIS);
	harris3D_detect.setInputCloud(cloud);
	pcl::PointCloud<pcl::PointXYZI> keypoints_temp;
	harris3D_detect.compute (keypoints_temp);
	pcl::copyPointCloud (keypoints_temp, *keypoints);

	double t2 = my_clock();
	if(D) std::cout << "[info] Detected " << keypoints->size() << " keypoints" << std::endl;
	if(D) std::cout << "[info] FeatureEstimation::detectKeypoints() took " << elapsed(t1,t2) << std::endl;
}



void FeatureEstimation::computeLocalDescriptors()
{
	if(D) std::cout << "[info] FeatureEstimation::computeLocalDescriptors(): radius = "
			<< descriptor_radius << std::endl;
	double t1 = my_clock();

#ifdef OMP
	pcl::FPFHEstimationOMP<PointT, NormalT, LocalDescriptorT> fpfh_estimation;
#else
	pcl::FPFHEstimation<PointT, NormalT, LocalDescriptorT> fpfh_estimation;
#endif
	fpfh_estimation.setSearchMethod (pcl::search::KdTree<PointT>::Ptr (new pcl::search::KdTree<PointT>));
	fpfh_estimation.setRadiusSearch (descriptor_radius);
	fpfh_estimation.setSearchSurface (cloud);
	fpfh_estimation.setInputNormals (normals);
	fpfh_estimation.setInputCloud (keypoints);
	fpfh_estimation.compute (*local_descriptors);
	///TODO: for nan values in the descriptor, we simple replace them with 0; a better way would be
	///      to remove these descriptors; we must, however, also remove the corresponding keypoints!!!
	for(size_t i = 0; i < local_descriptors->points.size(); ++i)
	{
		for(size_t j = 0; j < 33; ++j)
		{
			if(!pcl_isfinite(local_descriptors->points[i].histogram[j])) local_descriptors->points[i].histogram[j] = 0;
		}
	}
	double t2 = my_clock();
	if(D) std::cout << "[info] Computed " << local_descriptors->size() << " Local Descriptors" << std::endl;
	if(D) std::cout << "[info] FeatureEstimation::computeLocalDescriptor () took " << elapsed(t1,t2) << std::endl;
}

void FeatureEstimation::computeGlobalDescriptor()
{
	if(D) std::cout << "[info] FeatureEstimation::computeGlobalDescriptor()" << std::endl;
	double t1 = my_clock();
	pcl::VFHEstimation<PointT, NormalT, GlobalDescriptorT> vfh_estimation;
	vfh_estimation.setSearchMethod (pcl::search::KdTree<PointT>::Ptr (new pcl::search::KdTree<PointT>));
	vfh_estimation.setInputCloud (cloud);
	vfh_estimation.setInputNormals (normals);
	//GlobalDescriptorsPtr global_descriptor (new GlobalDescriptors);
	vfh_estimation.compute (*global_descriptor);
	double t2 = my_clock();
	if(D) std::cout << "[info] Computed " << global_descriptor->size() << " Global Descriptor" << std::endl;
	if(D) std::cout << "[info] FeatureEstimation::computeGlobalDescriptor () took " << elapsed(t1,t2) << std::endl;
}


void FeatureEstimation::saveAllData(std::string base_filename)
{
	double t1 = my_clock();
	if(D) std::cout << "[info] Saving all output data to \"" << base_filename << "_*\"" << std::endl;
	std::string output_filename;
	// Save original cloud as xyz_points.pcd
	output_filename = base_filename;
	output_filename.append ("_points.pcd");
	pcl::io::savePCDFile (output_filename, *cloud);
	if(D) pcl::console::print_info ("Saved original point cloud as %s\n", output_filename.c_str ());

	output_filename = base_filename;
	output_filename.append ("_normals.pcd");
	pcl::io::savePCDFile (output_filename, *normals);
	if(D) pcl::console::print_info ("Saved surface normals as %s\n", output_filename.c_str ());

	output_filename = base_filename;
	output_filename.append ("_keypoints.pcd");
	pcl::io::savePCDFile (output_filename, *keypoints);
	if(D) pcl::console::print_info ("Saved keypoints as %s\n", output_filename.c_str ());

	output_filename = base_filename;
	output_filename.append ("_localdesc.pcd");
	pcl::io::savePCDFile (output_filename, *local_descriptors);
	if(D) pcl::console::print_info ("Saved local descriptors as %s\n", output_filename.c_str ());

	output_filename = base_filename;
	output_filename.append ("_nxyz_vfh.pcd");
	pcl::io::savePCDFile (output_filename, *global_descriptor);
	if(D) pcl::console::print_info ("Saved global descriptor as %s\n", output_filename.c_str ());

	//added concatenation of normals with cloud 
	pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr concat_cloud (new pcl::PointCloud<pcl::PointXYZRGBNormal>);
	pcl::concatenateFields (*cloud, *normals, *concat_cloud);
	output_filename = base_filename;
	output_filename.append ("_nxyz.pcd");
	pcl::io::savePCDFile (output_filename, *concat_cloud);
	if(D) pcl::console::print_info ("Saved surface XYZRGB_normals as %s\n", output_filename.c_str ());
	double t2 = my_clock();
	if(D) std::cout << "[info] Saving took " << elapsed(t1,t2) << std::endl;
}


void FeatureEstimation::setSurfaceNormalsRadius(double radius)
{
	normals_surface_radius = radius;
}


void FeatureEstimation::setKeypointsRadius(double radius)
{
	harris_radius = radius;
}


void FeatureEstimation::setLocalDescriptorsRadius(double radius)
{
	descriptor_radius = radius;
}


PointCloudPtr FeatureEstimation::getPointCloud()
{
	return cloud;
}


SurfaceNormalsPtr FeatureEstimation::getSurfaceNormals()
{
	return normals;
}


PointCloudPtr FeatureEstimation::getKeypoints()
{
	return keypoints;
}


LocalDescriptorsPtr FeatureEstimation::getLocalDescriptors()
{
	return local_descriptors;
}


GlobalDescriptorsPtr FeatureEstimation::getGlobalDescriptor()
{
	return global_descriptor;
}


