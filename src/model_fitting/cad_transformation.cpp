#include "model_fitting/cad_transformation.h"
#include "model_fitting/load_clouds.h"

#include <sys/time.h>
#include <sstream>
#include <vector>
#include <algorithm>

#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include "pcl/point_cloud.h"
#include <pcl/visualization/point_cloud_handlers.h>





//#define D 0
#define S 0

double 
CAD_Transformation::my_clock()
{
  struct timeval tv;
  gettimeofday(&tv, NULL);
  return tv.tv_sec + (double)tv.tv_usec*1e-6;
}

std::string 
CAD_Transformation::elapsed(double start, double end) {
	std::ostringstream os;
	os << (end-start) * 1000 << "ms";
	return os.str();
}

CAD_Transformation::CAD_Transformation()
: D(0), src_Points(new PointCloud), best_index(0)
{
	final_matrix =  vtkSmartPointer<vtkMatrix4x4>::New();	
	transform_view 	= vtkSmartPointer<vtkTransform>::New();
	
	polydata = vtkSmartPointer<vtkPolyData>::New();
	
	if(D) std::cout << "[info] Constructor CAD_Transformation()" << std::endl;
}

CAD_Transformation::~CAD_Transformation()
{
  if(D) std::cout << "[info] Destructor CAD_Transformation()" << std::endl;

}

void CAD_Transformation::setD(int d)
{
	D = d;
}

void 
CAD_Transformation::setPointCloud(pcl::PointCloud<PointT>::Ptr input)
{
	src_Points = input;
}

void CAD_Transformation::setScene(PointCloudPtr input)
{
	scene = input;
}

void 
CAD_Transformation::setVTKCADModel(std::string path)
{
	vtkCADModel_Path = path;
}

void 
CAD_Transformation::setPoses(std::string path)
{
	poses_Path = path;
}

void
CAD_Transformation::setInitCADPose(std::string pose)
{
	
	if (D) std::cout << "[info] full pose_path: " << pose << std::endl;
	
	//TODO: asumend that path before seeked pose is "../views/toaster_" (length 17)
	//		or "../views/ikeamug_" (length 17)
	pose.erase(0,17);
	initCADPose = atoi(pose.c_str());
	if (D) std::cout << "[info] selected pose: "<< initCADPose << std::endl;

}
void CAD_Transformation::setViewNames(std::vector<std::string> views)
{
    view_names = views;
}


void 
CAD_Transformation::setFinalCADTransformation(Eigen::Matrix4f mat)
{
    for(size_t j = 0; j < 4; ++j)
    {
        for(size_t k = 0; k < 4; ++k)
        {

            double v = mat(j,k);
            final_matrix->SetElement(j,k,v);
        }
    }

    if(D) std::cout << "[info] loaded final_matrix: " << std::endl;
    if(D) 
    {	
        vtkIndent v;
        final_matrix->PrintSelf(std::cout,v);
    }
}

void CAD_Transformation::setInitCADPoses(std::vector<std::string> poses)
{
        for(size_t p = 0; p < poses.size(); ++p)
        {
            poses[p].erase(0,17);
            initCADPoses.push_back(atoi(poses[p].c_str()));
            if (D) std::cout << "[info] selected pose: "<< initCADPoses[p] << std::endl;
        }
}


void CAD_Transformation::setFreeSpaceValues(std::vector<double> values)
{
    fs_values = values;
}


void CAD_Transformation::setFinalCADTransformations(std::vector<Eigen::Matrix4f, Eigen::aligned_allocator< Eigen::Matrix4f > > matrices)
{
    eigen_matrices = matrices;
    if(D) std::cout << "[info] Adding all transformation matrices" << std::endl;
    for(size_t p = 0; p < matrices.size(); ++p)
    {
        vtkSmartPointer<vtkMatrix4x4>  matrix = vtkSmartPointer<vtkMatrix4x4>::New();	
        for(size_t j = 0; j < 4; ++j)
        {
            for(size_t k = 0; k < 4; ++k)
            {
                double v = matrices[p](j,k);
                matrix->SetElement(j,k,v);
            }
        }
        final_matrices.push_back(matrix);
        if(D) std::cout << "[info] added a matrix" << std::endl;

        if(D) 
        {	
            vtkIndent v;
            matrix->PrintSelf(std::cout,v);
        }
    }
}


void
CAD_Transformation::loadPoses(std::string path)
{
	int number_of_views = 0;
	
	std::ifstream in_file;
    in_file.open(path.c_str());
    in_file >> number_of_views;
    
    if (D) std::cout << "[info] loading " << number_of_views << " poses." << std::endl;
		if (number_of_views == 0)
			std::cout << "[WARNING] No poses found. Is the path to \"poses.csv\" correct?" << std::endl;
    for(size_t i = 0; i < number_of_views; ++i)
    {
        vtkSmartPointer<vtkMatrix4x4> m = vtkSmartPointer<vtkMatrix4x4>::New();
        for(size_t j = 0; j < 4; ++j)
        {
            for(size_t k = 0; k < 4; ++k)
            {
				double v;
				in_file >> v;
				m->SetElement(j,k,v);
            }
        }
        poses.push_back(m);
    }
    in_file.close();
    if(D) std::cout << "[info] read " << poses.size() << " poses." << std::endl;
	
}


void
CAD_Transformation::loadVTKCADModel(std::string path)
{
	 // Get all data from the file
    vtkSmartPointer<vtkGenericDataObjectReader> reader = 
        vtkSmartPointer<vtkGenericDataObjectReader>::New();
    reader->SetFileName(path.c_str());
    reader->Update();    
         
    // All of the standard data types can be checked and obtained like this:
    if(reader->IsFilePolyData())
    {
        if(D) std::cout << "[info] vtkCADModel: '" << path << "' contains polydata" << std::endl;
        polydata = reader->GetPolyDataOutput();
        if(D) std::cout << "[info] vtkCADModel has " << polydata->GetNumberOfPoints() << " points." << std::endl;
	}
}

void 
CAD_Transformation::applyTransformations()
{
	loadPoses(poses_Path);
	loadVTKCADModel(vtkCADModel_Path);
	
	vtkSmartPointer<vtkTransform> transform_final = vtkSmartPointer<vtkTransform>::New();
	transform_view->SetMatrix(poses[initCADPose]);
	transform_view->PostMultiply();
	transform_final->SetMatrix(final_matrix);
	transform_view->Concatenate(transform_final);	
	
	//TODO: add method to save final VTKTransform
		
}

void CAD_Transformation::applyAllTransformations()
{
    loadPoses(poses_Path);
    loadVTKCADModel(vtkCADModel_Path);

    for(size_t p = 0; p < final_matrices.size(); ++p)
    {
						if(D) std::cout << "[info] applying transformation " << p << std::endl;
						vtkSmartPointer<vtkTransform> transform_final = vtkSmartPointer<vtkTransform>::New();
						vtkSmartPointer<vtkTransform> temp_transform_view = vtkSmartPointer<vtkTransform>::New();
						temp_transform_view->SetMatrix(poses[initCADPoses[p]]);
						temp_transform_view->PostMultiply();
						transform_final->SetMatrix(final_matrices[p]);
						temp_transform_view->Concatenate(transform_final);	

						transform_views.push_back(temp_transform_view);
    }
}

    void 
CAD_Transformation::visualizeResults()
{
    pcl::visualization::PCLVisualizer vis = pcl::visualization::PCLVisualizer("Visualizer");
    if (D) std::cout << "called vis" << std::endl;

    vis.addPointCloud(src_Points);
    vis.addModelFromPolyData (polydata, transform_view, "mesh1", 0);
    //vis.setRepresentationToSurfaceForAllActors();
    //vis.addCoordinateSystem();
    vis.spin();	

    if (D) std::cout << "vis spin done" << std::endl;
}

bool comp(double i, double j) { return (i>j); }

void CAD_Transformation::rankTransformations()
{
		std::vector<double> fs_values_sorted = fs_values;
		//std::vector<int> fs_indices;
		std::sort(fs_values_sorted.begin(), fs_values_sorted.end(), comp);
		
		for(size_t i = 0; i < fs_values_sorted.size(); ++i)
		{
						std::cout << fs_values_sorted[i] << ": ";//hh << fs_values[i] << std::endl;
						for(size_t j = 0; j < fs_values.size(); ++j)
						{
										if(fs_values_sorted[i] == fs_values[j]) std::cout << j << std::endl;
										if(fs_values_sorted[i] == fs_values[j]) fs_indices.push_back(j);
										//fs_indices.push_back(j);
						}
		}
		
		best_index = fs_indices[0];

}

void 
CAD_Transformation::visualizeAllResults()
{
    if (D) std::cout << "called vis" << std::endl;
/*
		std::vector<double> fs_values_sorted = fs_values;
		//std::vector<int> fs_indices;
		std::sort(fs_values_sorted.begin(), fs_values_sorted.end(), comp);
		
		for(size_t i = 0; i < fs_values_sorted.size(); ++i)
		{
						std::cout << fs_values_sorted[i] << ": ";//hh << fs_values[i] << std::endl;
						for(size_t j = 0; j < fs_values.size(); ++j)
						{
										if(fs_values_sorted[i] == fs_values[j]) std::cout << j << std::endl;
										if(fs_values_sorted[i] == fs_values[j]) fs_indices.push_back(j);
										//fs_indices.push_back(j);
						}
		}
		
		best_index = fs_indices[0];

*/
		

    Eigen::Vector4f centroid;
    pcl::compute3DCentroid (*src_Points, centroid);
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_xyz_demean (new pcl::PointCloud<pcl::PointXYZRGB>);
    pcl::demeanPointCloud<pcl::PointXYZRGB> (*src_Points, centroid, *cloud_xyz_demean);

    int k = final_matrices.size();
    pcl::visualization::PCLVisualizer p = pcl::visualization::PCLVisualizer("Visualizer");
    int y_s = (int)floor (sqrt ((double)k));
    int x_s = y_s + (int)ceil ((k / (double)y_s) - y_s);
    double x_step = (double)(1 / (double)x_s);
    double y_step = (double)(1 / (double)y_s);
    int viewport = 0, l = 0, m = 1;
      for (int i = 0; i < k; ++i)
      //for (int i = k-1; i >= 0; --i)
      {
          std::ostringstream name;
          name << "particle " << fs_indices[i];
          std::ostringstream meshname;
          meshname << "mesh" << fs_indices[i];
          std::ostringstream fs_score;
          fs_score << "FreeSpace score: " << fs_values[fs_indices[i]];
          std::ostringstream partialview;
          partialview << "partialview" << fs_indices[i];
          p.createViewPort (l * x_step, m * y_step, (l + 1) * x_step, (m + 1) * y_step, viewport);
          l++;
          if (l >= x_s)
          {
              l = 0;
              m--;
          }
          PointCloudPtr partial_view = CloudIO::loadPoints(view_names[fs_indices[i]]);
          //PointCloudPtr pv_transformed (new PointCloud);
          pcl::PointCloud<pcl::PointXYZRGB>::Ptr pv_transformed (new PointCloud);
          pcl::transformPointCloud(*partial_view, *pv_transformed, eigen_matrices[fs_indices[i]]);

          //p.addPointCloud(cloud_xyz_demean, name.str(), viewport);
          p.addPointCloud(src_Points, name.str(), viewport);
          p.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 4, name.str());
          p.addModelFromPolyData (polydata, transform_views[fs_indices[i]], meshname.str(), viewport);
          //p.addModelFromPolyData (polydata, meshname.str(), viewport);
          p.addText (name.str (), 20, 10, 0, 1, 0, name.str (), viewport);
          p.addText (fs_score.str (), 20, 30, 1, 0, 0, fs_score.str (), viewport);
          p.resetCameraViewpoint(name.str());
          // make colorhandler
          //pcl::visualization::PointCloudColorHandlerCustom<PointCloud> single_color(basic_cloud_ptr, 0.0, 1.0, 0.0);

          pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZRGB> color(pv_transformed, 0, 255, 0);
          p.addPointCloud(pv_transformed, color, partialview.str(), viewport);
          //p.addCoordinateSystem(0.1);
          //p.setBackgroundColor(0.3, 0.3, 0.3, viewport);
          //p.setBackgroundColor(51, 102, 255, viewport);

      }

      /*
         if(D) std::cout << "[info] centroid: " << centroid(0) << " " << centroid(1) << " " << centroid(2) << std::endl;
         p.camera_.view[0]=centroid(0);
         p.camera_.view[1]=centroid(1);
         p.camera_.view[2]=centroid(2);
       */

      p.spin();	
      if(D) std::cout << "[info] spin finished" << std::endl;
}


void CAD_Transformation::visualizeBestResult()
{
		std::ostringstream name;
		name << "particle " << best_index;
		std::ostringstream meshname;
		meshname << "mesh" << best_index;
		std::ostringstream fs_score;
		fs_score << "FreeSpace score: " << fs_values[best_index];
		Eigen::Vector4f centroid;
		pcl::compute3DCentroid (*src_Points, centroid);
		pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_xyz_demean (new pcl::PointCloud<pcl::PointXYZRGB>);
		pcl::demeanPointCloud<pcl::PointXYZRGB> (*src_Points, centroid, *cloud_xyz_demean);

		pcl::visualization::PCLVisualizer p = pcl::visualization::PCLVisualizer("Visualizer");

		PointCloudPtr partial_view = CloudIO::loadPoints(view_names[best_index]);
		PointCloudPtr pv_transformed (new PointCloud);
		pcl::transformPointCloud(*partial_view, *pv_transformed, eigen_matrices[best_index]);

		//p.addPointCloud(cloud_xyz_demean, name.str(), viewport);
		p.addPointCloud(scene, "scene", 0);
		//p.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 4, name.str());
		p.addModelFromPolyData (polydata, transform_views[best_index], "mesh", 0);
		//p.addModelFromPolyData (polydata, meshname.str(), viewport);
		p.addText (name.str (), 20, 10, 0, 1, 0, name.str (), 0);
		p.addText (fs_score.str (), 20, 30, 1, 0, 0, fs_score.str (), 0);
		p.resetCameraViewpoint("scene");
		//p.addPointCloud(pv_transformed, partialview.str(), viewport);
		//p.addCoordinateSystem(0.1);
		//p.setBackgroundColor(0.3, 0.3, 0.3, viewport);


		p.spin();	
		if(D) std::cout << "[info] spin finished" << std::endl;

}



