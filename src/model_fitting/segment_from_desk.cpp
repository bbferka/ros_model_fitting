#include "model_fitting/segment_from_desk.h"
//#include "helpers.h"

#include <time.h>
#include <pcl/common/time.h>

#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>

#include <pcl/filters/passthrough.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/filters/voxel_grid.h>

#include <pcl/features/normal_3d.h>
#include <pcl/features/normal_3d_omp.h>

#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/common/angles.h> // for deg2rad
#include <pcl/filters/extract_indices.h>

#include <pcl/segmentation/extract_clusters.h>
#include <pcl/filters/project_inliers.h>

#include <pcl/surface/convex_hull.h>
#include <pcl/segmentation/extract_polygonal_prism_data.h>

#include <pcl/surface/mls.h>

#include <pcl/common/transforms.h>


#include <iostream>
#include <string>

//#define D 0
#define S 1
#define OMP

typedef pcl::PointXYZRGB PointT;
typedef pcl::PointXYZRGBNormal PointTNormal;


double SegmentFromDesk::my_clock()
{
  struct timeval tv;
  gettimeofday(&tv, NULL);
  return tv.tv_sec + (double)tv.tv_usec*1e-6;
}


std::string SegmentFromDesk::elapsed(double start, double end) {
	std::ostringstream os;
	os << (end-start) * 1000 << "ms";
	return os.str();
}


SegmentFromDesk::SegmentFromDesk()
: D(0), x_min(-1.0), x_max(1.0), y_min(-1.0), y_max(1.0), z_min(0.0), z_max(1.5),
  nr_neighbors(50), std_mul(3),
  voxel_size(0.01),
  search_radius(0.03),
  angle_weight(0.1), eps_angle(10), inlier_threshold(0.02), max_iterations(100),
  axis_x(1), axis_y(0), axis_z(0),
  min_size(10), conn_threshold(0.05), min_height(0.02), max_height(0.5),
  smooth_radius(0.02), min_plane_size(100), select_closest(false),
  cloud(new pcl::PointCloud<PointT>), cloud_filtered(new pcl::PointCloud<PointT>),
  cloud_downsampled(new pcl::PointCloud<PointT>), cloud_normals(new pcl::PointCloud<pcl::Normal>),
  cloud_plane(new pcl::PointCloud<PointT>), cloud_hull(new pcl::PointCloud<PointT>),
  coefficients_plane_best(new pcl::ModelCoefficients),
  max_size(0), max_cluster(-1), tree (new pcl::search::KdTree<PointT> ()),
  mls_points(new pcl::PointCloud<PointT>()), mls_points_rotated(new pcl::PointCloud<PointT> ()), mls_points_rotated_normals(new pcl::PointCloud<pcl::Normal>)
{
	if(D) std::cout << "[info] Constructor SegmentFromDesk()" << std::endl;
}


SegmentFromDesk::~SegmentFromDesk()
{
	if(D) std::cout << "[info] Destructor SegmentFromDesk()" << std::endl;
}

void SegmentFromDesk::setD(int d) {
	D = d;
}

void SegmentFromDesk::setParams(double voxel_size, double std_mul)
{
	this->voxel_size = voxel_size;
	this->std_mul = std_mul;
}


void SegmentFromDesk::setPointCloud(std::string name)
{
	double t1 = my_clock();
	if(D) std::cout << "[info] Loading point cloud: \"" << name << "\"";
	//cloud  = (new pcl::PointCloud<PointT>());
	pcl::PCDReader reader;
	reader.read(name, *cloud); // TODO parse PCD file name by extension
	double t2 = my_clock();
	if(D) std::cout << " with " << cloud->points.size() << " points in " << elapsed(t1,t2) << "." << std::endl;
}


void SegmentFromDesk::setPointCloud(pcl::PointCloud<PointT>::Ptr pointcloud)
{
	double t1 = my_clock();
	if(D) std::cout << "[info] Setting point cloud...";
	cloud = pointcloud;
	double t2 = my_clock();
	if(D) std::cout << " with " << cloud->points.size() << " points in " << elapsed(t1,t2) << "." << std::endl;
}


void SegmentFromDesk::FilterPointCloud()
{
	double t1 = my_clock();
	if(D) std::cout << "[info] Filtering point cloud ";
	/*
	//remove NaN-s
	std::vector<int> nans;
	pcl::removeNaNFromPointCloud(*cloud, *cloud_without_nans, nans);
	std::cerr << "PointCloud after nans have been removed: " << cloud_without_nans->points.size () << " data points." << std::endl;
	 */

#ifdef OMP
        ///TODO: doing the downsampling before the rest SIGNIFICANTLY speeds the whole thing up!! (nobrainer, really...)
        ///      evaluate the effects this has!
        pcl::VoxelGrid<PointT> vg;
        vg.setInputCloud (cloud);
        vg.setLeafSize (voxel_size, voxel_size, voxel_size);
        vg.filter (*cloud_downsampled);
        cloud = cloud_downsampled;
#endif
// set filter limits to vg!
	pcl::PassThrough<PointT> pass;
	pass.setInputCloud (cloud);
	pass.setFilterFieldName ("x");
	pass.setFilterLimits (x_min, x_max);
	pass.filter (*cloud_filtered);
	pass.setInputCloud (cloud_filtered);
	pass.setFilterFieldName ("y");
	pass.setFilterLimits (y_min, y_max);
	pass.filter (*cloud_filtered);
	pass.setInputCloud (cloud_filtered);
	pass.setFilterFieldName ("z");
	pass.setFilterLimits (z_min, z_max);
	pass.filter (*cloud_filtered);
	
	// Perform statistical noise removal
	if (nr_neighbors > 0 && std_mul > 0)
	{
			pcl::StatisticalOutlierRemoval<PointT> sor;
			sor.setInputCloud (cloud_filtered);
			sor.setMeanK (nr_neighbors);
			sor.setStddevMulThresh (std_mul);
			sor.filter (*cloud_filtered);
			if(D) std::cout << "w/ noise removal: ";
	}
	else
			if(D) std::cout << "w/o noise removal: ";

	double t2 = my_clock();
	if(D) std::cout << cloud_filtered->points.size () << " points left, took " << elapsed(t1,t2) << "." << std::endl;
	// Check
	if (cloud_filtered->points.size() == 0) ///TODO: raise exception
			std::cout << "[ERROR] No points remaining after filtering!" << std::endl; //return (0);

	if(S) t1 = my_clock();
	if(S) std::cout << "[note]Saving File Filtered Cloud...";
	if(S) pcl::io::savePCDFileASCII ("cloud_filtered.pcd", *cloud_filtered);
	if(S) t2 = my_clock();
	if(S) std::cout << " in " << elapsed(t1,t2) << "." << std::endl;


}


void SegmentFromDesk::DownsampleFilteredPointCloud()
{
	double t1 = my_clock();
	if(D) std::cout << "[info] Downsampling filtered point cloud... ";
	cloud_downsampled = cloud_filtered;
        /*
	if (voxel_size > 0)
	{
			cloud_downsampled = pcl::PointCloud<PointT>::Ptr (new pcl::PointCloud<PointT>);
			pcl::VoxelGrid<PointT> vg;
			vg.setInputCloud (cloud_filtered);
			vg.setLeafSize (voxel_size, voxel_size, voxel_size);
			vg.filter (*cloud_downsampled);
	}
	else
			std::cout << " skipped.";
                        */

	double t2 = my_clock();
	if(D) std::cout << cloud_downsampled->points.size() << " points left, took " << elapsed(t1,t2) << ". [IGNORE]" << std::endl;

	if(S) t1 = my_clock();
	if(S) std::cout << "[note]Saving File Downsampled Cloud...";
	if(S) pcl::io::savePCDFileASCII ("downsampled_cloud.pcd", *cloud_downsampled);
	if(S) t2 = my_clock();
	if(S) std::cout << elapsed(t1,t2) << std::endl;


}


void SegmentFromDesk::EstimateNormals()
{
	double t1 = my_clock();
	if(D) std::cout << "[info] Estimating normals for point cloud... ";
	// Estimating normals, so we can filter plane inliers better
#ifdef OMP
	pcl::NormalEstimationOMP<PointT, pcl::Normal> ne;
#else
	pcl::NormalEstimation<PointT, pcl::Normal> ne;
#endif
	//pcl::KdTreeFLANN<PointT>::Ptr tree (new pcl::KdTreeFLANN<PointT> ());
	//pcl::search::KdTree<PointT>::Ptr tree (new pcl::search::KdTree<PointT> ());
	ne.setInputCloud (cloud_downsampled);
	ne.setSearchSurface (cloud_filtered);
	//ne.setSearchMethod(tree);
	ne.setSearchMethod (tree);
	ne.setRadiusSearch (search_radius);
	ne.compute (*cloud_normals);

	pcl::PointCloud<PointTNormal>::Ptr cloud_normals_temp (new pcl::PointCloud<PointTNormal>);
	pcl::concatenateFields (*cloud_downsampled, *cloud_normals, *cloud_normals_temp);
	double t2 = my_clock();
	if(D) std::cout << " took " << elapsed(t1,t2) << "." << std::endl;

	if(S) t1 = my_clock();
	if(S) std::cout << "[note]Saving File Plane Cloud...";
	if(S) pcl::io::savePCDFileASCII ("normals_cloud.pcd", *cloud_normals_temp);
	if(S) t2 = my_clock();
	if(S) std::cout << " in " << elapsed(t1,t2) << "." << std::endl;

}


void SegmentFromDesk::DoSegmentation()
{
	if(D) std::cout << "[info] Doing segmentation...";
	// Create the segmentation object for the planar model and set all the parameters
	pcl::SACSegmentationFromNormals<PointT, pcl::Normal> seg;
	//seg.setModelType (pcl::SACMODEL_NORMAL_PARALLEL_PLANE); /// TODO: find out why this doesn't work, even though PARALLEL and NORMAL alone work...
//	seg.setModelType (pcl::SACMODEL_PARALLEL_PLANE);
	//seg.setModelType (pcl::SACMODEL_NORMAL_PLANE);
	seg.setModelType (pcl::SACMODEL_PLANE);
	seg.setNormalDistanceWeight (angle_weight); // 0.1 ???
	seg.setEpsAngle(pcl::deg2rad(eps_angle));
	seg.setOptimizeCoefficients (true);
	seg.setMethodType (pcl::SAC_RANSAC);
	seg.setMaxIterations (max_iterations);
	seg.setDistanceThreshold (inlier_threshold);
	Eigen::Vector3f ax (axis_x, axis_y, axis_z);
	seg.setAxis(ax);

	//extract all planes, disregard walls floor etc.
	//pcl::ModelCoefficients::Ptr coefficients_plane_best (new pcl::ModelCoefficients);
	pcl::PointIndices::Ptr inliers_plane_best (new pcl::PointIndices);
	//pcl::PointCloud<PointT>::Ptr cloud_plane (new pcl::PointCloud<PointT> ());
	pcl::ExtractIndices<PointT> extract;


	while(cloud_downsampled->points.size() > min_plane_size)
	{
		if(D) std::cout << "trying to obtain inliers" << std::endl;
		// Obtain the plane inliers and coefficients
		pcl::ModelCoefficients::Ptr coefficients_plane_current (new pcl::ModelCoefficients);
		pcl::PointIndices::Ptr inliers_plane_current (new pcl::PointIndices);
		seg.setInputNormals (cloud_normals);
		seg.setInputCloud (cloud_downsampled);
		if(D) std::cout << "trying to segment" << std::endl;
		seg.segment (*inliers_plane_current, *coefficients_plane_current);
		std::cerr<<"NR. OF INLIERS FOUND: "<<inliers_plane_current->indices.size()<<std::cerr;		
		if(D) std::cout << "segmentation succeeded" << std::endl;
		// Check
		if (inliers_plane_current->indices.size() < min_plane_size)
				break;

		if(D) std::cout << "obtained inliers" << std::endl;

		// Flip plane normal towards viewpoint
		PointT p = cloud_downsampled->points[inliers_plane_current->indices[0]];
		float dot_product = p.x * coefficients_plane_current->values[0] +
				p.y * coefficients_plane_current->values[1] +
				p.z * coefficients_plane_current->values[2];
		if (dot_product > 0)
		{
				coefficients_plane_current->values[0] *= -1;
				coefficients_plane_current->values[1] *= -1;
				coefficients_plane_current->values[2] *= -1;
				coefficients_plane_current->values[3] *= -1;
		}

		if(D) std::cout << "fitted plane " << std::endl;

		//check angle with y
		float angle = acos(coefficients_plane_current->values[1]);
		if( (angle> M_PI/2) )  // if current plane is thought to be the best:D
		{
				coefficients_plane_best = coefficients_plane_current;
				inliers_plane_best = inliers_plane_current;

				// Extract the planar inliers from the input cloud
				extract.setInputCloud (cloud_downsampled);
				extract.setIndices (inliers_plane_current);
				extract.setNegative (false);
				extract.filter (*cloud_plane);
				break;
		}

		if(D) std::cout << "extracted inliers" << std::endl;

		//removing plane form the scan
		extract.setInputCloud (cloud_downsampled);
		extract.setIndices (inliers_plane_current);
		extract.setNegative (true);
		extract.filter (*cloud_downsampled); // plane outliers
		pcl::ExtractIndices<pcl::Normal> extract_normals;
		extract_normals.setInputCloud (cloud_normals);
		extract_normals.setIndices (inliers_plane_current);
		extract_normals.setNegative(true);
		extract_normals.filter (*cloud_normals); // corresponding normals
	}
	// Check
	if(D) std::cout << "PointCloud representing the planar component: " << cloud_plane->points.size () << " data points." << std::endl;
	if (cloud_plane->points.size () == 0  )
			std::cout << "[ERROR] No points in cloud_plane!" << std::endl; //return(0);
	if(S) std::cout << "[note]Saving File Plane Cloud..." << std::endl;
	if(S) pcl::io::savePCDFileASCII ("plane_cloud.pcd", *cloud_plane);

}

void SegmentFromDesk::DoEuclidianClustering()
{
	if(D) std::cout << "[info] Doing euclidian clustering... ";
        double t1 = my_clock();
	// Euclidian cluster
	//std::vector<pcl::PointIndices> clusters;
	pcl::EuclideanClusterExtraction<PointT> ece;
	ece.setInputCloud (cloud_plane);
	ece.setMinClusterSize (min_size);
	ece.setClusterTolerance (conn_threshold);
	ece.extract (clusters);
	std::cout << " found " << clusters.size() << " cluster(s)." << std::endl;

	// Find the biggest cluster
	//unsigned int max_size = 0;
	//int max_cluster = -1;
	for (unsigned int i = 0; i < clusters.size(); ++i)
		if (clusters[i].indices.size()>max_size)
		{
			max_size = clusters[i].indices.size();
			max_cluster = i;
		}

	//project plane+cluster
	pcl::ProjectInliers<PointT> proj;
	proj.setModelType (pcl::SACMODEL_PLANE);
	proj.setInputCloud (cloud_plane);
	proj.setIndices(boost::make_shared <std::vector<int> >(clusters[max_cluster].indices));
	proj.setModelCoefficients (coefficients_plane_best);
	proj.filter (*cloud_plane);
        double t2 = my_clock();
        if(D) std::cout << elapsed(t1,t2) << std::endl;

}

void SegmentFromDesk::CreateHull()
{
	if(D) std::cout << "[info] Creating convex hull... ";
        double t1 = my_clock();
	// Create a Convex Hull representation of the projected inliers
	//pcl::PointCloud<PointT>::Ptr cloud_hull (new pcl::PointCloud<PointT>);
	pcl::ConvexHull<PointT> chull;
	chull.setInputCloud (cloud_plane);
	//chull.setAlpha (0.1);
	chull.reconstruct (*cloud_hull);

	//contracting the hull
	Eigen::Vector4f centroid;
	pcl::compute3DCentroid(*cloud_hull, centroid);
	for(unsigned int i = 0; i<cloud_hull->points.size();++i)
	{
		Eigen::Vector4f p = cloud_hull->points[i].getVector4fMap();
		Eigen::Vector4f diff = p-centroid;
		p = centroid+diff*0.9;
		cloud_hull->points[i].x = p[0];
		cloud_hull->points[i].y = p[1];
		cloud_hull->points[i].z = p[2];
	}
	if(S) std::cout << "[note]Saving Cloud Hull..." << std::endl;
	if(S) pcl::io::savePCDFileASCII ("hull_cloud.pcd", *cloud_hull);

	//polygonal prism: min_height -> max_height
	pcl::PointIndices::Ptr cloud_prism (new pcl::PointIndices);
	pcl::ExtractPolygonalPrismData <PointT> extract_prism;
	extract_prism.setInputPlanarHull(cloud_hull);
	extract_prism.setInputCloud(cloud_filtered);
	extract_prism.setHeightLimits(min_height,max_height);
	extract_prism.segment(*cloud_prism);

	//extracting relevant indices form filtered cloud
	pcl::ExtractIndices<PointT> extract;
	extract.setInputCloud (cloud_filtered);
	extract.setIndices (cloud_prism);
	extract.setNegative (false);
	extract.filter (*cloud_filtered);
        double t2 = my_clock();
        if(D) std::cout << elapsed(t1,t2) << std::endl;
	if(D) std::cout << " found " << cloud_hull->points.size() << " points." << std::endl;


}

void SegmentFromDesk::FindObjectInHull()
{
        if(D) std::cout << "[info] Finding object in hull..." << std::endl;
        double t1 = my_clock();
	//find the bigest cluster in the remaining cloud and we presume that that is the object we are looking for
	//std::vector<pcl::PointIndices> clusters_final;
	pcl::EuclideanClusterExtraction<PointT> ece;
	ece.setInputCloud(cloud_filtered);
	ece.setMinClusterSize (min_size);
	ece.setClusterTolerance (conn_threshold);
	ece.extract (clusters_final);
	if(D) std::cout << "Number of clusters: " << clusters_final.size() << std::endl;
	max_size = 0;
	max_cluster = -1;
	for (unsigned int i = 0; i < clusters_final.size(); ++i)
		if (clusters_final[i].indices.size()>max_size)
		{
			max_size = clusters_final[i].indices.size();
			max_cluster = i;
		}

	//extract the biggest cluster from the poit cloud
	pcl::ExtractIndices<PointT> extract;
	extract.setInputCloud(cloud_filtered);
	extract.setIndices(boost::make_shared <std::vector<int> >(clusters_final[max_cluster].indices));
	extract.setNegative(false);
	extract.filter(*cloud_filtered);

	//smooth the cloud for later purposes
	mls_points = cloud_filtered;
	pcl::PointCloud<pcl::PointNormal>::Ptr mls_normals (new pcl::PointCloud<pcl::PointNormal> ());
	
	if(smooth_radius>0)
	{
		std::cout<<"MLS POINTS SIZE: "<<mls_points->points.size()<<std::endl;		
		tree->setInputCloud(cloud_filtered);
		pcl::MovingLeastSquares<PointT, pcl::PointNormal> mls;
		//mls.setComputeNormals (true);		
		//mls.setOutputNormals (mls_normals);
		
		// Set parameters
		mls.setInputCloud (cloud_filtered);
		mls.setPolynomialFit (true);
		mls.setSearchMethod (tree);
		mls.setSearchRadius (smooth_radius);
		mls.process(*mls_normals);
	}
	else
	{
		tree->setInputCloud(cloud_filtered);
		pcl::NormalEstimation<PointT, pcl::Normal> ne;
		ne.setInputCloud(cloud_filtered);
		ne.setSearchMethod(tree);
		ne.setRadiusSearch (search_radius);
		//ne.compute(*mls_normals);
	}
	if(D) std::cout << "[note] Postprocessing done" << std::endl;
	// Concatenate fields for saving

	pcl::PointCloud<PointTNormal>::Ptr mls_cloud (new pcl::PointCloud<PointTNormal> ());;
	pcl::concatenateFields (*mls_points, *mls_normals, *mls_cloud);


	Eigen::Vector3f v1(1,0,0);
	Eigen::Vector3f v2(coefficients_plane_best->values[0],  coefficients_plane_best->values[1], coefficients_plane_best->values    [2]);
	Eigen::Vector3f v3 = v2.cross(v1);
	Eigen::Matrix4f t_matrix = Eigen::Matrix4f::Zero(4,4);
	t_matrix.row(0)<<v1.transpose(),0;
	t_matrix.row(1)<<v3.transpose(),0;
	t_matrix.row(2)<<v2.transpose(),0;
	t_matrix(3,3) = 1;

	Eigen::Affine3f m(t_matrix);
	if (D) std::cout << t_matrix << std::endl;


	//pcl::PointCloud<PointT>::Ptr mls_points_rotated(new pcl::PointCloud<PointT> ());

	if (S) pcl::io::savePCDFileASCII ("falschrum.pcd", *mls_points);
	//changed method call from PCL 1.2 to newer version (getTransformedPointCloud to transformPointCloud)
	pcl::transformPointCloud(*mls_points,*mls_points_rotated,m);
        double t2 = my_clock();
        if(D) std::cout << "Took " << elapsed(t1,t2) << std::endl;

	std::string append = smooth_radius>0 ? "_smooth.pcd": "_normal.pcd";
	if(S) pcl::io::savePCDFileASCII ("res.pcd", *mls_points_rotated);
        if(D) std::cout << "[info] Final Object has " << mls_points_rotated->points.size() << " points." << std::endl;



}


pcl::PointCloud<pcl::Normal>::Ptr SegmentFromDesk::getSegmentedCloudNormals()
{
	double t1 = my_clock();
	if(D) std::cout << "[info] Estimating normals for final point cloud... ";
	// Estimating normals, so we can filter plane inliers better
	pcl::NormalEstimation<PointT, pcl::Normal> ne;
	//pcl::KdTreeFLANN<PointT>::Ptr tree (new pcl::KdTreeFLANN<PointT> ());
	//pcl::search::KdTree<PointT>::Ptr tree (new pcl::search::KdTree<PointT> ());
	ne.setInputCloud (mls_points_rotated);
	//ne.setSearchSurface (cloud_filtered);
	//ne.setSearchMethod(tree);
	ne.setSearchMethod (tree);
	ne.setRadiusSearch (search_radius);
	ne.compute (*mls_points_rotated_normals);

	//pcl::PointCloud<PointTNormal>::Ptr cloud_normals_temp (new pcl::PointCloud<PointTNormal>);
	//pcl::concatenateFields (*cloud_downsampled, *cloud_normals, *cloud_normals_temp);
	double t2 = my_clock();
	if(D) std::cout << " took " << elapsed(t1,t2) << "." << std::endl;

	return mls_points_rotated_normals;

	//if(S) t1 = my_clock();
	//if(S) std::cout << "[note]Saving File Plane Cloud...";
	//if(S) pcl::io::savePCDFileASCII ("normals_cloud.pcd", *cloud_normals_temp);
	//if(S) t2 = my_clock();
	//if(S) std::cout << " in " << elapsed(t1,t2) << "." << std::endl;

}

pcl::PointCloud<PointT>::Ptr SegmentFromDesk::getInputPointCloud()
{
	return cloud;
}

pcl::PointCloud<PointT>::Ptr SegmentFromDesk::getSegmentedCloud()
{
    //return mls_points_rotated;
			pcl::PointCloud<PointT>::Ptr result = pcl::PointCloud<PointT>::Ptr (new pcl::PointCloud<PointT>);
			pcl::VoxelGrid<PointT> vg;
			vg.setInputCloud (mls_points);
			vg.setLeafSize(voxel_size, voxel_size, voxel_size);
			vg.filter (*result);
			if(S) pcl::io::savePCDFileASCII ("falschrum_ds.pcd", *result);
		return result;
}


pcl::PointCloud<PointT>::Ptr SegmentFromDesk::getFilteredCloud()
{
	return cloud_downsampled;
}

void SegmentFromDesk::execute()
{
    double t1 = my_clock();
    if(D) std::cout << "[info] Segmenting object from desk..." << std::endl;
    FilterPointCloud();
    DownsampleFilteredPointCloud();
    EstimateNormals();
    DoSegmentation();
    DoEuclidianClustering();
    CreateHull();
    FindObjectInHull();
    double t2 = my_clock();
    if(D) std::cout << "[info] All done. Took " << elapsed(t1,t2) << std::endl;

}
